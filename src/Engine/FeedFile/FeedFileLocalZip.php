<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedFile;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedController;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedLogger;
use Rhubarb\Scaffolds\FeedImport\Exceptions\FeedException;
use Rhubarb\Scaffolds\FeedImport\Exceptions\FileException;

class FeedFileLocalZip extends FeedFileLocal
{
    /**
     * Add required settings
     */
    public function __construct()
    {
        parent::__construct();

        $this->requiredSettings[ ] = "ZipFile";
        $this->requiredSettings[ ] = "DataFileInZip";
    }

    /**
     * @param string $file
     *
     * @return string
     * @throws FeedException
     */
    protected function getFile( $file )
    {
        $extractedPath = APPLICATION_ROOT_DIR . '/' . FeedController::$settings[ "FilePath" ] . "fresh/extracted/";
        $this->unzipFile( $file, $extractedPath );
        $this->filesToArchive[ ] = $file;

        if ( FeedController::$settings[ "DataFileInZip" ] ) {
            return $extractedPath . FeedController::$settings[ "FileName" ];
        } else {
            return FeedController::$settings[ "FileToProcess" ];
        }
    }

    protected function getPreparedFilePath()
    {
        return APPLICATION_ROOT_DIR . '/' . FeedController::$settings[ "FilePath" ] . "fresh/" . FeedController::$settings[ 'ZipFile' ];
    }

    protected function unzipFile( $file, $extractedPath )
    {
        $zip = new \ZipArchive();
        $result = $zip->open( $file, \ZipArchive::CHECKCONS );
        if ($result !== true) {
            throw new FileException( "Error opening zip file {$file}.", FeedLogger::ERROR );
        }

        // get a list of files in the zip pre-unzipping
        $filesInZip = array();
        for ($i = 0; $i < $zip->numFiles; $i++) {
            $this->filesToDelete[ ] = $zip->getNameIndex( $i );
        }

        if (!file_exists( $extractedPath )) {
            mkdir( $extractedPath );
        }
        $this->dirsToDelete[ ] = $extractedPath;

        if (!$zip->extractTo( $extractedPath )) {
            throw new FileException( "Error extracting zip file {$file}.", FeedLogger::ERROR );
        }
    }

    protected function checkFile($file)
    {
        parent::checkFile($file);

        if (!FeedController::$settings["DataFileInZip"]) {
            $this->filesToArchive[] = $file;
        }
    }

}
