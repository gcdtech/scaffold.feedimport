<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedFile;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedController;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedLogger;
use Rhubarb\Scaffolds\FeedImport\Exceptions\FeedException;
use Rhubarb\Scaffolds\FeedImport\Exceptions\FileException;

class FeedFileFTP extends FeedFile
{
    /**
     * Add required settings
     */
    public function __construct()
    {
        parent::__construct();

        $this->removeExistingFile = true;

        $this->requiredSettings[] = 'FTPServer';
        $this->requiredSettings[] = 'FTPFile';
        $this->requiredSettings[] = 'RemoteUsername';
        $this->requiredSettings[] = 'RemotePassword';
        $this->requiredSettings[] = 'FTPPassiveMode';
    }

    /** @var resource The FTP connection stream. Should not be accessed directly but using $this->checkConnection() */
    private $connection;

    /**
     * @return resource
     * @throws FeedException Failure to connect should halt the entire feed
     */
    protected function checkConnection()
    {
        if ($this->connection === null) {
            $this->connection = @ftp_connect(FeedController::$settings[ 'FTPServer' ]);
            if ($this->connection) {
                if (ftp_login(
                    $this->connection,
                    FeedController::$settings[ 'RemoteUsername' ],
                    FeedController::$settings[ 'RemotePassword' ]
                )) {
                    if (FeedController::$settings[ "FTPPassiveMode" ]) {
                        if (ftp_pasv($this->connection, true) == false) {
                            FeedController::$logger->logEvent(FeedLogger::WARNING_GCD, 'Could not enter passive mode');
                        }
                    }
                } else {
                    ftp_close($this->connection);
                    throw new FeedException('FTP user details rejected', FeedLogger::ERROR);
                }
            } else {
                throw new FeedException(
                    "Couldn't open FTP connection: " . FeedController::$settings[ 'FTPServer' ],
                    FeedLogger::ERROR
                );
            }
        } else {
            // Test connection is still alive
            if (ftp_nlist($this->connection, '.') === false) {
                // If a list operation fails we can assume the connection has dropped - attempt reconnect
                FeedController::$logger->logEvent(FeedLogger::INFO_GCD, 'FTP server went away, reconnecting');
                $this->connection = null;

                return $this->checkConnection();
            }
        }

        return $this->connection;
    }

    protected function getFileList()
    {
        return [ FeedController::$settings[ 'FTPFile' ] ];
    }

    /**
     * Fetches the target file from the remote server
     *
     * @param string $file
     *
     * @return string The current file should be skipped
     * @throws FileException
     */
    protected function getFile( $file )
    {
        $connection = $this->checkConnection();

        if (ftp_get(
            $connection,
            FeedController::$settings[ 'FileToProcess' ],
            './' . $file,
            FTP_ASCII
        )) {
            FeedController::$logger->logEvent(
                FeedLogger::INFO,
                'FTP download successful: ' . FeedController::$settings[ 'FTPServer' ] . '/' . FeedController::$settings[ 'FTPFile' ] . ' saved to ' . FeedController::$settings[ 'FileToProcess' ]
            );
        } else {
            throw new FileException(
                'FTP download failed: From ' . FeedController::$settings[ 'FTPServer' ] . '/' . FeedController::$settings[ 'FTPFile' ] . ' to ' . FeedController::$settings[ 'FileToProcess' ],
                FeedLogger::ERROR
            );
        }
        $this->filesToArchive[] = FeedController::$settings[ 'FileToProcess' ];

        return FeedController::$settings[ 'FileToProcess' ];
    }

    /**
     * Closes the ftp connection
     */
    public function cleanup()
    {
        if ($this->connection !== null) {
            ftp_close($this->connection);
        }
    }

}
