<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedFile;

class FeedFilePost extends FeedFileLocal
{

    /**
     * Saves data from a POST stream into a file according to
     * the FileToProcess setting
     *
     * @param string $file
     *
     * @return string
     * @throws \Rhubarb\Scaffolds\FeedImport\Exceptions\FeedException
     */
    protected function getFile($file)
    {
        $data = file_get_contents('php://input');

        return $this->putFileContents($data);
    }
}