<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedFile;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedControl;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedController;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedLogger;
use Rhubarb\Scaffolds\FeedImport\Exceptions\FeedException;
use Rhubarb\Scaffolds\FeedImport\Exceptions\FileException;

abstract class FeedFile extends FeedControl
{
    /**
     * Indicates that a file is ready (indicates if it should be archived)
     *
     * @var bool
     */
    protected $fileReady = false;

    protected $archiveFile = '';

    /** @var array Files to be added to the zip archive after the feed is processed */
    protected $filesToArchive = [];

    /** @var array Files to be deleted (not archived) after the feed is processed */
    protected $filesToDelete = [];

    /** @var array Directories to be deleted (not archived) after the feed is processed */
    protected $dirsToDelete = [];

    /**
     * Fetches a file to the working directory
     *
     * @abstract
     *
     * @param string $file The file to process
     *
     * @return string The local file path where the target file to be read is now stored
     */
    abstract protected function getFile($file);

    /**
     * Overridden to add settings
     */
    public function __construct()
    {
        parent::__construct();

        $this->requiredSettings[] = 'FileName';
        $this->requiredSettings[] = 'FileAlwaysRequired';
    }

    /**
     * @return string The file path to be passed to the parser
     * @throws FeedException
     */
    public function handleFeedFiles()
    {
        $this->environmentChecks();
        $filesToFetch = $this->getFileList();
        $filesPassedToReader = 0;
        foreach ($filesToFetch as $file) {
            try {
                $localFiles = glob($this->getFile($file));
                foreach ($localFiles as $localFile) {
                    try {
                        $this->checkFile($localFile);
                        $filesPassedToReader++;
                        FeedController::$logger->logEvent(FeedLogger::INFO_GCD, "Reading File: {$localFile}");
                        FeedController::$reader->read($localFile);
                    } catch (FileException $fileException) {
                        $this->logFileException($fileException);
                    }
                }
            } catch (FileException $fileException) {
                $this->logFileException($fileException);
            }
        }

        if ($filesPassedToReader === 0) {
            throw new FeedException('No files to read', $this->getLogLevelForNoResults());
        }

        $this->cleanup();
    }

    private function getLogLevelForNoResults()
    {
        $suppress = false;
        if (isset(FeedController::$settings['SuppressNoDataErrors'])) {
            $suppress = FeedController::$settings['SuppressNoDataErrors'];
        }

        if (isset(FeedController::$settings['FileAlwaysRequired'])) {
            $suppress = !FeedController::$settings['FileAlwaysRequired'];
        }

        $logLevel = FeedLogger::ERROR_GCD;
        if ($suppress) {
            $logLevel = FeedLogger::INFO_GCD;
        }

        return $logLevel;
    }

    private function logFileException(FileException $ex)
    {
        FeedController::$logger->logEvent($ex->getMessage(), $this->getLogLevelForNoResults());
    }

    /**
     * @return array
     */
    protected function getFileList()
    {
        return glob($this->getPreparedFilePath());
    }

    /**
     * The file path to be passed to the parser
     *
     * @return string
     */
    protected function getPreparedFilePath()
    {
        return FeedController::$settings['FileToProcess'];
    }

    /**
     * Pre-processing check of environment for feed files
     */
    protected function environmentChecks()
    {
        $this->checkDirectoryStructure();

        // Do archive housekeeping
        $removed = $this->doHouseKeeping();
        if ($removed > 0) {
            if ($removed > 1) {
                $removed .= " archives";
            } else {
                $removed .= " archive";
            }
            
            FeedController::$logger->logEvent(FeedLogger::INFO_GCD, "{$removed} removed in housekeeping");
        }
    }

    /**
     * Checks ClassifiedFeedController::$settings[ "FileToProcess"]
     *
     * @param string $file File in the working directory to check
     *
     * @throws FeedException
     */
    protected function checkFile($file)
    {
        FeedController::$logger->logEvent(FeedLogger::INFO_GCD, "Checking File: {$file}");

        if (file_exists($file)) {
            if (is_readable($file)) {
                if (filesize($file) < 1) {
                    throw new FeedException("File check failed: No data in " . $file, $this->getLogLevelForNoResults());
                }
                FeedController::$logger->logEvent(FeedLogger::INFO, "File Check OK: " . $file);
            } else {
                throw new FeedException("Could not open file: " . $file, FeedLogger::ERROR_GCD);
            }
        } else {
            throw new FeedException("File not found: " . $file, $this->getLogLevelForNoResults());
        }
    }

    /**
     * Writes $data to ClassifiedFeedController::$settings[ "FileToProcess"]
     *
     * @param string $data Data to write to file
     *
     * @return string
     * @throws FeedException if data could not be written to the File
     */
    protected function putFileContents($data)
    {
        $fileParts = explode(".", FeedController::$settings["FileToProcess"]);
        $file = $fileParts[0] . "." . $fileParts[1];
        $count = 1;

        while (file_exists($file)) {
            $file = $fileParts[0] . $count . "." . $fileParts[1];
            $count++;
        }

        if (file_put_contents($file, $data) !== false) {
            clearstatcache();
            FeedController::$logger->logEvent(FeedLogger::INFO, "Data written to file: " . $file);
            $this->setFilePermissions();
        } else {
            throw new FeedException("Could not write data to file: " . $file, FeedLogger::ERROR_GCD);
        }

        return $this->filesToArchive[] = $file;
    }

    /**
     * Archives and deletes processed files
     */
    public function cleanup()
    {
        $this->archive();
        $this->cleanupFilesAndDirs();
    }

    /**
     *  deletes files that are listed in filesToDelete
     */
    protected function cleanupFilesAndDirs()
    {
        foreach ($this->filesToDelete as $file) {
            @unlink($file);
            if (file_exists($file)) {
                FeedController::$logger->logEvent(FeedLogger::WARNING_GCD, "Feed file {$file} was not deleted.");
            }
        }

        foreach ($this->dirsToDelete as $dir) {
            exec('rm -rf ' . realpath($dir));
            if (file_exists($dir)) {
                FeedController::$logger->logEvent(FeedLogger::WARNING_GCD, "Feed directory {$dir} was not deleted.");
            }
        }
    }

    /**
     * Archives the current file (FileToProcess setting) to the archive directory
     */
    protected function archive()
    {
        if (count($this->filesToArchive) > 0) {
            $archiveDirectory = APPLICATION_ROOT_DIR . '/' . FeedController::$settings['FilePath'] . 'archive';

            if (file_exists($archiveDirectory) == false) {
                mkdir($archiveDirectory, 0777, true);
                if (file_exists($archiveDirectory)) {
                    FeedController::$logger->logEvent(FeedLogger::INFO,
                        "Created archive folder: $archiveDirectory");
                } else {
                    FeedController::$logger->logEvent(FeedLogger::WARNING_GCD,
                        "Could not create archive folder: $archiveDirectory");
                }
            }

            // just use one zip per feed run...
            if ($this->archiveFile == '') {
                $this->archiveFile = $archiveDirectory . '/' . date('Y-m-d-h-i-s-') . $this->sanitiseFilename(FeedController::$settings['FileName']) . '-' . FeedController::$feed->ProcessID . '.zip';
            }

            // open the archive (create if not already there)
            $zip = new \ZipArchive();
            $zip->open($this->archiveFile, \ZipArchive::CREATE);

            foreach ($this->filesToArchive as $file) {
                $zip->addFile($file, basename($file));
                // Add the file to the list of files to be deleted
                $this->filesToDelete[] = $file;
            }

            if ($zip->close()) {
                FeedController::$logger->logEvent(FeedLogger::INFO,
                    'Successfully archived: ' . implode(', ', $this->filesToArchive) . " to {$this->archiveFile}");
            } else {
                FeedController::$logger->logEvent(FeedLogger::WARNING_GCD,
                    "Could not create archive: {$this->archiveFile}");
            }
        }
    }

    protected function sanitiseFilename($filename)
    {
        $filename = preg_replace('/[^A-z0-9._-]/', '-', $filename);
        return $filename;
    }

    /**
     * Compresses a file using GZip compression
     *
     * @param string $file
     * @param bool   $deleteOriginal
     * @param string $saveTo
     */
    public function compressFile($file, $deleteOriginal = false, $saveTo = "") 
    {
        $data = file_get_contents($file);
        $data = gzcompress($data, 9);

        if ($saveTo == "") {
            $saveTo = $file . ".gz";
        }

        if (file_put_contents($saveTo, $data) != false) {
            clearstatcache();

            // Removed the original file if required
            $deletedOriginal = "";
            if ($deleteOriginal == true) {
                $deletedOriginal = " - Original file could not be removed";
                if (unlink($file)) {
                    $deletedOriginal = " - Original file removed";
                }
            }

            $size = self::describeBytes(filesize($saveTo));
            FeedController::$logger->logEvent(FeedLogger::INFO_GCD, "{$file} compressed and saved to {$saveTo} ({$size}){$deletedOriginal}");
        } else {
            FeedController::$logger->logEvent(FeedLogger::WARNING_GCD, "{$file} could not be saved to {$saveTo}");
        }

        FeedController::$logger->logEvent(FeedLogger::INFO_GCD, "");
    }

    /**
     * Ensures that a directory (and all of it's parents) are created
     * If not, creates the directory and CHMOD to 777
     *
     * @param mixed $dir
     *
     * @throws FeedException
     */
    protected function checkDirectoryExists($dir) 
    {
        if (file_exists($dir) == false) {
            if (mkdir($dir, 0777, true)) {
                FeedController::$logger->logEvent(FeedLogger::INFO, "Created directory structure: $dir");
            } else {
                throw new FeedException("Failed to create directory structure: $dir", FeedLogger::ERROR_GCD);
            }
        }
    }

    /**
     * Ensures that all required directory structures are in place for feed processing
     */
    public function checkDirectoryStructure()
    {
        $this->checkDirectoryExists(APPLICATION_ROOT_DIR . '/' . FeedController::$settings["FilePath"] . "archive/");
        $this->checkDirectoryExists(APPLICATION_ROOT_DIR . '/' . FeedController::$settings["FilePath"] . "logs/");
        $this->checkDirectoryExists(APPLICATION_ROOT_DIR . '/' . FeedController::$settings["FilePath"] . "fresh/");
    }

    /**
     * CHMOD file identified by FileToProcess setting to 777
     *
     * @throws FeedException If permissions cannot be set
     */
    protected function setFilePermissions()
    {
        if (chmod(FeedController::$settings["FileToProcess"], 0777)) {
            FeedController::$logger->logEvent(FeedLogger::INFO_GCD,
                "File permissions set: " . FeedController::$settings["FileToProcess"]);
        } else {
            throw new FeedException("Could not set file permissions: " . FeedController::$settings["FileToProcess"],
                FeedLogger::ERROR_GCD);
        }
    }

    /**
     * Describes a number of bytes
     *
     * @static
     *
     * @param int $bytes
     *
     * @return string Description of passed bytes
     */
    public
    static function describeBytes(
        $bytes
    ) {
        $units = array(
            " Bytes",
            "KB",
            "MB",
            "GB",
            "TB",
            "PB"
        );

        $scale = 0;
        while ($bytes > 1023) {
            $bytes = $bytes / 1024;
            $scale++;
        }

        $bytes = round($bytes, 2);

        return $bytes . $units[$scale];
    }

    /**
     * Handles housekeeping for feed data files
     *
     * @param int $numberToKeep
     *
     * @return int
     */
    protected function doHouseKeeping($numberToKeep = 10)
    {
        $allFiles = glob(APPLICATION_ROOT_DIR . '/' . FeedController::$settings["FilePath"] . "archive/*.*");
        $filesToKeep = array_slice($allFiles, $numberToKeep * -1);
        $fileToRemove = array_diff($allFiles, $filesToKeep);
        $removed = 0;

        if (empty($fileToRemove) == false) {
            foreach ($fileToRemove as $file) {
                if (@unlink($file)) {
                    $removed++;
                }
            }
        }

        return $removed;
    }

    public function checkoutFiles()
    {
        try {
            $filesToFetch = $this->getFileList();
            foreach ($filesToFetch as $file) {
                $this->getFile($file);
            }
        } catch (\Exception $ex) {
            throw new \Exception("Error checking out files" . $ex);
        }
    }
}
