<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedFile;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedController;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedLogger;

class FeedFileURL extends FeedFile
{
    /**
     * Add required settings
     */
    public function __construct()
    {
        parent::__construct();

        $this->requiredSettings[] = 'FileURL';
    }

    /**
     * @return array
     */
    protected function getFileList()
    {
        return array( FeedController::$settings[ 'FileURL' ] );
    }

    /**
     * @param string $path
     *
     * @return string
     * @throws \Rhubarb\Scaffolds\FeedImport\Exceptions\FeedException
     */
    protected function getFile($path)
    {
        FeedController::$logger->logEvent( FeedLogger::INFO, "Retrieving data from $path" );
        $data = $this->getRemoteFileContents( $path );

        if (!$data) {
            FeedController::$logger->logEvent( FeedLogger::ERROR,
                    'Could not load data from ' . FeedController::$settings[ 'FileURL' ] );
        }

        return $this->putFileContents( $data );
    }

    /**
     * @param      $url
     * @param null $username
     * @param null $password
     * @param int  $timeout
     *
     * @return bool|mixed
     */
    function getRemoteFileContents($url, $username = null, $password = null, $timeout = 900)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        // For compatibility with HTTPS
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        // Use auth if required
        if ($username != null && $password != null) {
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        }

        // Give up immediately on error
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);

        // Follow redirects
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        // add a timeout to avoid excessive waiting
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

        // Return data, don't print
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // Ignore headers
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // Accept all encoding (for servers that can give us the lovely speediness of gzip)
        curl_setopt($ch, CURLOPT_ENCODING, "");

        // Use binary transfer mode
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);

        $contents = curl_exec($ch);

        // return false if no data
        if (!$contents) {
            return false;
        }

        curl_close($ch);

        return $contents;
    }

    /**
     * @param     $url
     * @param     $post
     * @param int $timeout
     *
     * @return bool|mixed
     */
    function getRemoteFileContentsWithPost($url, $post, $timeout = 900)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        // For compatibility with HTTPS
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        // Give up immediately on error
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);

        // Follow redirects
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        // add a timeout to avoid excessive waiting - 15 mins
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

        // Return data, don't print
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // Ignore headers
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // Accept all encoding (for servers that can give us the lovely speediness of gzip)
        curl_setopt($ch, CURLOPT_ENCODING, "");

        // Use binary transfer mode
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);

        $contents = curl_exec($ch);

        // return false if no data
        if (!$contents) {
            return false;
        }

        curl_close($ch);

        return $contents;
    }
}