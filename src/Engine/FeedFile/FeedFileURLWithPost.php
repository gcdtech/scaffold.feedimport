<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedFile;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedController;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedLogger;

class FeedFileURLWithPost extends FeedFileURL
{
    /**
     * Add required settings
     */
    public function __construct()
    {
        parent::__construct();

        $this->requiredSettings[ ] = 'PostDataJSON';
    }

    /**
     * @param string $file
     *
     * @return string
     * @throws \Rhubarb\Scaffolds\FeedImport\Exceptions\FeedException
     */
    protected function getFile( $file )
    {
        $postData = FeedController::$settings[ 'PostDataJSON' ];
        $jsonData = json_decode( $postData );

        if($jsonData == null){
            $jsonData = json_decode( str_replace( "'", "\"", $postData ) );
        }
        if($postData != '' && $jsonData != ''){
            $postData = $jsonData;
        }
        $data = $this->getRemoteFileContentsWithPost( FeedController::$settings[ 'FileURL' ], $postData );

        if(!$data) {
            FeedController::$logger->logEvent( FeedLogger::ERROR, 'Could not load data from ' . FeedController::$settings[ 'FileURL' ] );
        }

        return $this->putFileContents( $data );
    }
}