<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedFile;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedController;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedLogger;

class FeedFileURLSecure extends FeedFileURL
{
    /**
     * Add required settings
     */
    public function __construct()
    {
        parent::__construct();

        $this->requiredSettings[] = 'RemoteUsername';
        $this->requiredSettings[] = 'RemotePassword';
    }

    /**
     * Gets a file from a secure URL and saves it locally according to
     * the FileToProcess setting
     *
     * @param string $file
     *
     * @return string
     * @throws \Rhubarb\Scaffolds\FeedImport\Exceptions\FeedException
     */
    protected function getFile($file)
    {
        $data = $this->getRemoteFileContents(FeedController::$settings[ 'FileURL' ],
            FeedController::$settings[ 'RemoteUsername' ],
            FeedController::$settings[ 'RemotePassword' ]);

        if (!$data) {
            FeedController::$logger->logEvent(FeedLogger::ERROR,
                'Could not load data from ' . FeedController::$settings[ 'FileURL' ] . ' as user ' . FeedController::$settings[ 'RemoteUsername' ]);
        }

        return $this->putFileContents($data);
    }
}