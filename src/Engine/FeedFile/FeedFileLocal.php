<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedFile;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedController;

class FeedFileLocal extends FeedFile
{

    /**
     * Not really needed for this one!
     *
     * @param string $file
     *
     * @return string
     */
    protected function getFile($file)
    {
        return $this->filesToArchive[] = $file;
    }
}