<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedReader;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedControl;

abstract class FeedReader extends FeedControl
{
    abstract public function read( $path );

    public function getRequiredSettings()
    {
        return parent::getRequiredSettings();
    }

    public function getController()
    {
        parent::getController();
    }

}