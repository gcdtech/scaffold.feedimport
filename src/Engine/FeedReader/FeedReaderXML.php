<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedReader;

use Rhubarb\Crown\Xml\Node;
use Rhubarb\Crown\Xml\XmlParser;

abstract class FeedReaderXML extends FeedReader
{
    protected $readFields = [ ];

    /**
     * @param string $path
     */
    public function read( $path )
    {
        $xmlParser = new XmlParser( $path );
        $this->addNodeHandlers( $xmlParser );
        $xmlParser->Parse();
    }

    /**
     * @param XmlParser $parser
     */
    abstract protected function addNodeHandlers( XmlParser $parser );

    /**
     * Helper method designed for adding all of a node's attributes/node values to the read fields
     *
     * @param Node   $node
     * @param string $fieldPrefix
     */
    protected function addNodeToReadFields( Node $node, $fieldPrefix = '' )
    {
        if ($fieldPrefix != '') {
            $fieldPrefix = $fieldPrefix . '_';
        }

        $readNodeMethod = "readNode{$fieldPrefix}{$node->name}";
        if (method_exists( $this, $readNodeMethod )) {
            $this->$readNodeMethod( $node, $fieldPrefix );
        } else {
            foreach ($node->attributes as $attribute => $value) {
                $fieldName = $fieldPrefix . $node->name . '_' . $attribute;
                $this->addToReadFields( $fieldName, $value );
            }

            if (sizeof( $node->children ) > 0) {
                foreach ($node->children as $child) {
                    $this->addNodeToReadFields( $child, $fieldPrefix . $node->name );
                }
            } else {
                $this->addToReadFields( $fieldPrefix . $node->name, $node->text );
            }
        }
    }

    /**
     * @param string $fieldName
     * @param mixed  $fieldValue
     */
    protected function addToReadFields( $fieldName, $fieldValue )
    {
        $readMethodName = "read{$fieldName}";
        if (method_exists( $this, $readMethodName )) {
            $this->$readMethodName( $fieldValue );
        } else {
            $this->readFields[ $fieldName ] = $fieldValue;
        }
    }
}