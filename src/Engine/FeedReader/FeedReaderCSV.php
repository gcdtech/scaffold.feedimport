<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedReader;


use Rhubarb\Crown\DataStreams\CsvStream;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedController;

class FeedReaderCSV extends FeedReader
{
    function __construct()
    {
        parent::__construct();

        $this->requiredSettings[ ] = 'HasCustomHeaders';
    }


    /**
     * @param $path
     */
    public function read( $path )
    {
        $stream = new CsvStream( $path );

        $this->readItems( $stream );
    }

    protected function readItems( CsvStream $stream )
    {
        if (!FeedController::$settings[ 'HasCustomHeaders' ]) {
            $stream->readHeaders();
        }

        while ($data = $stream->readNextItem()) {
            FeedController::$parser->parse( $data );
        }
    }
}