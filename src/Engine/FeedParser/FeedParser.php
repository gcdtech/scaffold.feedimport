<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedParser;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedControl;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedController;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedLogger;
use Rhubarb\Scaffolds\FeedImport\Exceptions\FeedException;
use Rhubarb\Scaffolds\FeedImport\Exceptions\FeedParserException;
use Rhubarb\Scaffolds\FeedImport\Exceptions\ProcessorException;
use Rhubarb\Scaffolds\FeedImport\Model\FeedLog;

abstract class FeedParser extends FeedControl
{
    /**
     * Key Value mappings, for... uhh mapping keys to values.
     *
     * @var array
     */
    protected $dataMappings = [ ];

    /**
     * @var array In case we need to do access the passed through data for mapping
     */
    protected $rawData = [];

    /**
     * Number of items that have been parsed
     *
     * @var int
     */
    public $parsedItems = 0;

    function __construct()
    {
        parent::__construct();

        if (!empty( FeedController::$settings[ 'DataMappings' ] )) {
            $this->dataMappings = array_merge( $this->dataMappings, FeedController::$settings[ 'DataMappings' ] );
        }
    }

    /**
     * @param array $recordData
     *
     * @throws FeedException
     */
    public function parse( $recordData )
    {
        if (empty( $recordData )) {
            return;
        }
            $this->rawData = $recordData;

            FeedController::$logger->logEvent( FeedLogger::INFO, "Parsing item #" . $this->parsedItems );

            $this->prepareDataObjects();

            foreach ($recordData as $fieldName => $fieldValue) {

                if (isset( $this->dataMappings[ $fieldName ] )) {
                    $this->parseField( $fieldValue, $fieldName );
                }
            }

            FeedController::$logger->logEvent( FeedLogger::INFO, "Parsing item #" . $this->parsedItems . " complete - Property Reference: " . $this->property->PropertyID );

            $this->parsedItems++;

        try {
            FeedController::$logger->logEvent( FeedLogger::INFO, "Processing property - reference: " . $this->property->PropertyID );
            FeedController::$processor->process( $this->dataObjects );
            FeedController::$logger->logEvent( FeedLogger::INFO, "Processing property complete." );
        } catch ( ProcessorException $ex ) {
            FeedController::$logger->logEvent( FeedLogger::WARNING, $ex->getMessage() );
        }
    }

    /** @var array $dataObjects The objects to be passed to the processor */
    protected $dataObjects = [ ];

    /**
     * Sets up data objects
     */
    abstract protected function prepareDataObjects();

    /**
     * Parses data for a specific field if the method exists.
     *
     * @param Mixed  $data
     * @param string $field
     *
     * @return array Parsed data
     */
    protected function parseField( $data, $field )
    {
        // Map the field from the source file according to our mappings
        if (isset( $this->dataMappings[ $field ] )) {
            $field = $this->dataMappings[ $field ];
        }

        $parseMethodName = "parse{$field}";
        if (method_exists( $this, $parseMethodName )) {
            $this->$parseMethodName( $data );
        } else {
            $this->setDataObjectValue( $data, $field );
        }
    }

    abstract function setDataObjectValue( $val, $field );
}
