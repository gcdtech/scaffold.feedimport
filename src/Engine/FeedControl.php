<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine;

/**
 * Class FeedControl
 *
 * @package Rhubarb\Scaffolds\FeedImport\Engine
 */
abstract class FeedControl
{
    protected $requiredSettings = array();

    function __construct()
    {
    }

    public function getRequiredSettings()
    {
        return array_unique( array_filter( $this->requiredSettings ) );
    }

    public function removeRequiredSettings( $settings )
    {
        if (is_array( $settings ) && !empty( $settings )) {
            $this->requiredSettings = array_diff(
                $this->requiredSettings,
                $settings
            );
        }
    }

    public function getController()
    {
    }

    public function prepare(){}
    public function cleanup(){}

}