<?php
namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedReporter;

use Rhubarb\Crown\Sendables\Email\EmailRecipient;
use Rhubarb\Scaffolds\FeedImport\Email\FeedEmail;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedControl;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedController;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedLogger;

/**
 * Class FeedReporter
 *
 * @package Rhubarb\Scaffolds\FeedImport\Engine\FeedReporter
 */
class FeedReporter extends FeedControl
{
    /**
     * Reports to be sent. Populated in constructor
     *
     * @var array
     */
    protected $reports = array();

    /**
     * Address to send emails from
     *
     * @var string
     */
    protected $emailFrom;

    /**
     * Stores teh name of the website sending the report
     *
     * @var string
     */
    protected $siteName;

    /**
     * Construct object
     */
    public function __construct()
    {
        parent::__construct();

        $this->emailFrom = FeedController::$systemSettings[ "reportingEmailSender" ];
        $this->siteName = FeedController::$systemSettings[ "siteName" ];

        if (!empty( FeedController::$feed->Reports )) {
            $this->reports = FeedController::$feed->Reports;
        }
    }

    /**
     * Process items from the logger. Saves the report and
     * emails the report of the the different groups
     */
    public function sendReports()
    {
        if (empty( $this->reports ) == false) {
            foreach ($this->reports as $emailAddress => $report) {
                $methodName = "report" . $report;
                if (method_exists( $this, $methodName )) {
                    FeedController::$logger->logEvent( FeedLogger::INFO_GCD,
                        "Checking {$report} report for {$emailAddress}" );
                    $this->$methodName( $emailAddress );
                } else {
                    FeedController::$logger->logEvent( FeedLogger::WARNING_GCD,
                        "Reporter failed to initialise: {$report} - Reporter not declared" );
                }
            }
        }
    }

    /**
     * Returns array of all logs with log codes at least $level
     *
     * @param int  $level
     * @param bool $includeInternal TRUE: include GCD items in the report. FALSE: exclude GCD items
     *
     * @return array
     */
    protected function getLogs( $level, $includeInternal = false )
    {
        $returnLogs = array();

        $logs = FeedController::$logger->getLogItems();

        foreach ($logs as $log) {
            if ($log[ "Code" ] >= $level) {
                // Skip internals if not required
                if ($includeInternal == false && ( ( $log[ "Code" ] % 2 ) == 0 )) {
                    continue;
                }

                $returnLogs[ ] = $log;
            }
        }

        return $returnLogs;
    }

    /**
     * A report of all events (excluding internal events)
     *
     * @param $email
     */
    public function reportAll( $email )
    {
        $logs = $this->getLogs( FeedLogger::INFO );
        $reportText = "The " . FeedController::$settings[ "Name" ] . " feed has been run. Please see the full report below";
        if (count( $logs ) != 0) {
            $this->generateReport( $reportText, $logs, $email );
        } else {
            FeedController::$logger->logEvent( FeedLogger::INFO_GCD, "Nothing to report" );
        }
    }

    /**
     * A report of all events (including internal events)
     *
     * @param $email
     */
    public function reportAllGCD( $email )
    {
        $logs = $this->getLogs( FeedLogger::INFO_GCD, true );
        $reportText = "The " . FeedController::$settings[ "Name" ] . " feed has been run. Please see the full report below";
        if (count( $logs ) != 0) {
            $this->generateReport( $reportText, $logs, $email );
        } else {
            FeedController::$logger->logEvent( FeedLogger::INFO_GCD, "Nothing to report" );
        }
    }

    /**
     * A report of all warnings and errors (including internal events)
     *
     * @param $email
     */
    public function reportGCD( $email )
    {
        $logs = $this->getLogs( FeedLogger::WARNING_GCD, true );
        $reportText = "Problems have occurred while running the " . FeedController::$settings[ "Name" ] . " feed.";
        if (count( $logs ) != 0) {
            $this->generateReport( $reportText, $logs, $email );
        } else {
            FeedController::$logger->logEvent( FeedLogger::INFO_GCD, "Nothing to report" );
        }
    }

    /**
     * A report of all no data errors (including internal events)
     *
     * @param $email
     */
    public function reportNoData( $email )
    {
        $logs = $this->getLogs( FeedLogger::ERROR_NO_DATA, true );
        $reportText = "A 'No Data' error has occurred while running the " . FeedController::$settings[ "Name" ] . " feed.";
        if (count( $logs ) != 0) {
            $this->generateReport( $reportText, $logs, $email );
        } else {
            FeedController::$logger->logEvent( FeedLogger::INFO_GCD, "Nothing to report regarding 'No Data'" );
        }
    }

    /**
     * Creates and sends an email report
     *
     * @param mixed $headerText
     * @param mixed $reportData
     * @param mixed $emailAddress
     */
    protected function generateReport( $headerText, $reportData, $emailAddress )
    {
        $reportHTML = <<<HTML
		<p>
			$headerText
		</p>
HTML;
        if (count( $reportData ) > 0) {
            $reportHTML .= <<<HTML
			<table style="padding:3px;">
				<tr style="font-weight: bold;">
					<th style="width: 100px;">Logged</th>
					<th style="width: 600px;">Message</th>
				</tr>
HTML;
            foreach ($reportData as $reportItem) {
                $level = FeedLogger::getStatusClass( $reportItem[ "Code" ] );
                $message = FeedLogger::parseMessageAsHTML( $reportItem[ "Message" ] );

                $col1Style = "font-weight: bold;";
                $col2Style = "";

                if (stripos( $level, "error" ) !== false) {
                    $bg = "background-color: #fcc;";
                    $border = "border: 2px solid #f00;";

                    $col1Style .= $bg . $border;
                    $col2Style .= $bg . $border;
                } elseif (stripos( $level, "warning" ) !== false) {
                    $bg = "background-color: #ff9;";
                    $border = "border: 2px solid #f90;";

                    $col1Style .= $bg . $border;
                    $col2Style .= $bg . $border;
                } else {
                    $bg = "background-color: #cfc;";
                    $border = "border: 2px solid #3c0;";

                    $col1Style .= $bg . $border;
                    $col2Style .= $border;
                }

                $reportHTML .= <<<HTML
				<tr>
					<td style="{$col1Style}">{$level}</td>
					<td style="{$col2Style}">{$message}</td>
				</tr>
HTML;
            }

            $reportHTML .= <<<HTML
			</table>
HTML;
        }
        $email = new FeedEmail( "Feed Report", $this->emailFrom, $reportHTML );
		$email->setSender( $this->emailFrom );

        $emailRecipient = new EmailRecipient( $emailAddress );
        $email->addRecipient( $emailRecipient );
        $email->send();

        FeedController::$logger->logEvent( FeedLogger::INFO_GCD, "Report sent to {$emailAddress}" );
    }

    /**
     * Email the Report to the group
     *
     * @param mixed $group
     * @param mixed $reportData
     */
    protected function sendEmail( $group, $reportData )
    {
        $emailTo = FeedController::$settings[ $group . "Email" ];
        $email = new FeedEmail( $group, $reportData, $this->emailFrom );
        $email->addRecipient( $emailTo );
        $email->send();
    }
}