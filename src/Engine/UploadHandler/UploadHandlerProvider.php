<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\UploadHandler;

use Rhubarb\Scaffolds\FeedImport\Model\Feed;

/**
 *
 */
interface UploadHandlerProvider
{

    /**
     * @param Feed $feed
     *
     * @return UploadHandler
     */
    public function getUploadHandler( Feed $feed );

}