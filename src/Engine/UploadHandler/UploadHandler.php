<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\UploadHandler;

use Rhubarb\Leaf\Views\View;
use Rhubarb\Scaffolds\FeedImport\Model\Feed;

abstract class UploadHandler extends View
{
    /** @var Feed */
    protected $feed;

    function __construct( Feed $feed )
    {
        $this->feed = $feed;
    }

    abstract protected function handleUpload();

    final protected function onBeforePrintViewContent()
    {
        $this->handleUpload();
    }

    abstract protected function printResponse();

    final protected function printViewContent()
    {
        $this->printResponse();
    }

}