<?php
/**
 *
 * Manages the execution of a feed
 *
 * @author Scott
 * Date: 30/06/15
 */

namespace Rhubarb\Scaffolds\FeedImport\Engine;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedFile\FeedFile;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedParser\FeedParser;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedProcessor\FeedProcessor;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedReader\FeedReader;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedReporter\FeedReporter;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes\FeedSettingType;
use Rhubarb\Scaffolds\FeedImport\Exceptions\FeedException;
use Rhubarb\Scaffolds\FeedImport\Exceptions\FeedRunException;
use Rhubarb\Scaffolds\FeedImport\Model\Feed;

/**
 * Class FeedController
 *
 * @package Rhubarb\Scaffolds\FeedImport\Engine
 */
class FeedController
{
    /** @var Feed */
    public static $feed;

    /** @var FeedLogger */
    public static $logger;

    /** @var FeedFile */
    public static $file;

    /** @var FeedReader */
    public static $reader;

    /** @var FeedParser */
    public static $parser;

    /**
     * Settings retrieved for this feed
     *
     * @var array
     */
    public static $settings;

    /** @var FeedProcessor */
    public static $processor;

    /** @var FeedReporter */
    public static $reporter;

    /**
     * Settings read from the feed setting file
     *
     * @var array
     */
    public static $systemSettings;

    /**
     * Enables the tracking of process information for the execution of a feed
     *
     * @var bool
     */
    public $enableProcessTracking = true;

    /**
     * Default values for settings in case they are blank
     *
     * @var array
     */
    protected static $defaultSettings = array(
        "FileName" => "data.csv"
    );

    /**
     * @param $feedId
     */
    public function __construct($feedId)
    {
        try {
            self::$feed = Feed::loadReadyOrPendingFeed($feedId);

            $this->initialise();
        } catch (FeedException $ex) {
            print $ex->getMessage();
            // throw it on up to the background task
            throw new FeedRunException($ex->getMessage());
        }
    }

    protected function initialise()
    {
        $this->loadSettings();

        self::$logger = new FeedLogger();

        try {
            self::$file = self::$feed->getControlObject('File');
            self::$file->prepare();

            self::$reader = self::$feed->getControlObject('Reader');
            self::$reader->prepare();

            self::$parser = self::$feed->getControlObject('Parser');
            self::$parser->prepare();

            self::$processor = self::$feed->getControlObject('Processor');
            self::$processor->prepare();

            self::$reporter = self::$feed->getControlObject('Reporter');
            self::$reporter->prepare();
        } catch (\Exception $exception) {
            self::$logger->logEvent($exception->getCode(), $exception->getMessage());
        }
    }

    protected function loadSettings()
    {
        // Get system settings from the ini file
        self::$systemSettings = self::getSystemSettings();

        // Handle all the rest of the settings
        $settings = self::$defaultSettings;

        $settingsField = self::$feed->FeedSettings;

        if (empty($settingsField) == false) {
            $parsedSettings = FeedSettingType::parseSettings($settingsField);
            $settings = array_merge($settings, $parsedSettings);
        }

        $settings["FeedID"] = self::$feed->FeedID;
        $settings["Name"] = self::$feed->FeedName;
        $settings["IDTag"] = self:: $feed->IDTag;
        $settings["DataMappings"] = self::$feed->DataMappings;

        // Lets invent some defaults
        if (array_key_exists("FilePath", $settings) == false) {
            $settings["FilePath"] = "feeds/{$settings[ "IDTag" ]}/";
        }

        if (array_key_exists("FileToProcess", $settings) == false) {
            $settings["FileToProcess"] = APPLICATION_ROOT_DIR . "/{$settings[ "FilePath" ]}fresh/{$settings[ "FileName" ]}";
        }

        self::$settings = $settings;
    }

    public static function getSystemSettings()
    {
        return parse_ini_file(APPLICATION_ROOT_DIR."/settings/feeds.ini");
    }

    public function process()
    {
        $taskStartTime = date("Y-m-d H:i:s");

        try {
            self::$feed->Status = "Running";
            FeedController::$logger->logEvent(FeedLogger::INFO_GCD, "Feed Started - " . $taskStartTime);
            self::$feed->save();

            $startTime = microtime(true);

            self::$file->handleFeedFiles();

            self::$reader->cleanup();
            self::$parser->cleanup();
            self::$processor->cleanup();

            $endTime = microtime(true) - $startTime;

            $log = self::$logger->dbLog;
            $log->CompletedTime = $endTime;
            $log->save();

            $timeUnit = "second";
            if ($endTime > 120) {
                $endTime = $endTime / 60;
                $timeUnit = "minute";
            }
            $endTime = round($endTime, 2);

            self::$logger->logEvent(FeedLogger::INFO_GCD, "Feed processed in {$endTime} {$timeUnit}s");
        } catch (FeedException $feedEx) {
            self::$logger->logEvent($feedEx->getCode(), $feedEx->getMessage());
        } catch (\Exception $exception) {
            self::$logger->logEvent($exception->getCode(), $exception->getMessage());
        }

        if (!isset(self::$settings['ChangedPropertiesCount']) ||
            (isset(self::$settings['ChangedPropertiesCount']) && self::$settings['ChangedPropertiesCount'] == 0)
        ) {
            self::$feed->LastStartTime = self::$feed->TaskStartTime = $taskStartTime;
        }

        self::$feed->setNextRunTime();

        try {
            self::$file->cleanup();

        } catch (FeedException $ex) {
            self::$logger->logEvent($ex->getCode(), $ex->getMessage());
        } catch (\Exception $exception) {
            self::$logger->logEvent($exception->getCode(), $exception->getMessage());
        }

        self::$reporter->sendReports();
        self::$reporter->cleanup();

        self::$logger->handleLogs();
        self::$logger->cleanup();

        self::$feed->LastRunCreatedCount = $log->Updated;
        self::$feed->LastRunSkippedCount = $log->Skipped;
        self::$feed->LastRunState = $log->Status;
        self::$feed->Status = "Ready";
        self::$feed->RetryCount = 0;
        self::$feed->save();
    }

    function __destruct()
    {
        // if the class is destructing and we haven't handled the logs, do some handling!
        if( !self::$logger->handledLogs ) {
            self::$logger->handleLogs();
        }
    }

    public function checkoutFilesFromURLs()
    {
        self::$file->checkoutFiles();
    }
}
