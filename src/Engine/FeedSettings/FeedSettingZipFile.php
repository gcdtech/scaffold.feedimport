<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings;

class FeedSettingZipFile extends FeedSettingFileName
{
    /**
     * No default value
     * @var string
     */
    protected $default = "";

    /**
     * Value to use as a placeholder for form inputs
     * @var string
     */
    protected $placeholder = "archive.zip";
}