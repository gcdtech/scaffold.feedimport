<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes\FeedSettingTypeText;

/**
 * Class ClassifiedFeedSettingSchedule
 *
 * @package Propertynews\Feeds\Feeds\FeedSettings
 */
class FeedSettingSchedule extends FeedSettingTypeText
{

}