<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes\FeedSettingTypeText;

class FeedSettingPostDataJSON extends FeedSettingTypeText
{
    /**
     * This setting needs a value
     * @var bool
     */
    protected $required = true;

    /**
     * Value to use as a placeholder for form inputs
     * @var string
     */
    protected $placeholder = "{ 'key1' : 'value1', 'key2' : 'value2' }";
}