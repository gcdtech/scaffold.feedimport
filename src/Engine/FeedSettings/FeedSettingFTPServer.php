<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes\FeedSettingTypeText;

class FeedSettingFTPServer extends FeedSettingTypeText
{
    /**
     * Value to use as a placeholder for form inputs
     * @var string
     */
    protected $placeholder = "ftp.example.com";
}