<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings;

class FeedSettingFTPFile extends FeedSettingFileName
{
    /**
     * No default value
     * @var string
     */
    protected $default = "";

    /**
     * Value to use as a placeholder for form inputs
     * @var string
     */
    protected $placeholder = "directory/example.txt";
}