<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes\FeedSettingTypeText;

class FeedSettingRemotePassword extends FeedSettingTypeText
{
    /**
     * Value to use as a placeholder for form inputs
     * @var string
     */
    protected $placeholder = "password";
}