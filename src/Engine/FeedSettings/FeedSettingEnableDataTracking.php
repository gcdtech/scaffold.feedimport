<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings;


use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes\FeedSettingTypeBoolean;

class FeedSettingEnableDataTracking extends FeedSettingTypeBoolean
{
    /**
     * The default value for this setting,
     * used if no value is supplied
     *
     * @var bool
     */
    protected $default = true;

}