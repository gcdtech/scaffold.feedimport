<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings;


use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes\FeedSettingTypeBoolean;

class FeedSettingFileAlwaysRequired extends FeedSettingTypeBoolean
{
    /**
     * Files should be required by default
     *
     * @var bool
     */
    protected $default = true;
}