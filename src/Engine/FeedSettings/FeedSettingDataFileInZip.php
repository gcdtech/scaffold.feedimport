<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes\FeedSettingTypeBoolean;

class FeedSettingDataFileInZip extends FeedSettingTypeBoolean
{
    /**
     * No default value
     * @var string
     */
    protected $default = "true";
}