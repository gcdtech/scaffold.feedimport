<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes;

use Rhubarb\Scaffolds\FeedImport\Helpers\FeedStringTools;

abstract class FeedSettingTypeText extends FeedSettingType
{
    /**
     * Value to use as a placeholder for form inputs
     *
     * @var string
     */
    protected $placeholder = "";

    /**
     * The default value for this setting,
     * used if no value is supplied
     *
     * @var bool
     */
    protected $default = "";

    /**
     * Gets a text input for this setting
     *
     * @return string HTML
     */
    public function getInput()
    {
        $required = "";
        if ($this->required) {
            $required = "required";
        }

        $title = FeedStringTools::UpperCamelCaseToWords( $this->name );

        return <<<HTML
        <div class="setting-selection-body">
            <label for="{$this->name}" class="setting-selection {$required}">{$title}:</label>
            <input type="text" name="FeedSettings[{$this->name}]" id="{$this->name}" value="{$this->value}" class="setting-selection {$required}" placeholder="{$this->placeholder}" />
        </div>
HTML;
    }

    /**
     * Allows for parsing of dates in file names
     * eg file[date:d-m-Y].csv
     *
     * @return string Parsed setting value
     */
    public function parseValue()
    {
        if (preg_match( "#\[date:(.+?)\]#i", $this->value, $matches )) {
            $this->value = str_replace( $matches[ 0 ], date( $matches[ 1 ] ), $this->value );
        }

        return $this->value;
    }
}