<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes;

class FeedSettingTypeArray extends FeedSettingType
{
    /**
     * Method stub, should be implemented for all feed setting types.
     *
     * @return mixed
     */
    public function getInput()
    {

        $existingHtml = "";

        foreach ($this->parseValue() as $key => $val) {
            $existingHtml .= <<<HTML
            <div class="key-val">
                <input type="text" id="{$this->name}[]" name="FeedSettings[{$this->name}][keys][]" value="{$key}">
                <input type="text" id="{$this->name}[]" name="FeedSettings[{$this->name}][values][]" value="{$val}">
                <a href="#" class="{$this->name}keyRemove">Remove</a>
            </div>
HTML;
        }

        if (!$existingHtml && $this->default) {
            foreach ($this->default as $key => $val) {
                $existingHtml .= <<<HTML
            <div class="key-val">
                <input type="text" id="{$this->name}[]" name="FeedSettings[{$this->name}][keys][]" value="{$key}">
                <input type="text" id="{$this->name}[]" name="FeedSettings[{$this->name}][values][]" value="{$val}">
                <a href="#" class="{$this->name}keyRemove">Remove</a>
            </div>
HTML;
            }
        }

        if (!$existingHtml && $this->placeholder) {
            foreach ($this->placeholder as $key => $val) {
                $existingHtml .= <<<HTML
                <div class="key-val">
                    <input type="text" id="{$this->name}[]" name="FeedSettings[{$this->name}][keys][]" placeholder="{$key}">
                    <input type="text" id="{$this->name}[]" name="FeedSettings[{$this->name}][values][]" placeholder="{$val}">
                    <a href="#" class="{$this->name}keyRemove">Remove</a>
                </div>
HTML;
            }
        }

        return <<<HTML
        <div class="setting-selection-body">

        <input type="text" id="{$this->name}[]" name="FeedSettings[{$this->name}]" style="display: none;">
        <xmp id="{$this->name}_placeholderKeySettings" style="display: none;">
            <div class="key-val" style="width: 100%;">
                <input type="text" id="{$this->name}[]" name="FeedSettings[{$this->name}][keys][]">
                <input type="text" id="{$this->name}[]" name="FeedSettings[{$this->name}][values][]">
                <a href="#" class="{$this->name}keyRemove">Remove</a>
            </div>
        </xmp>
        <label class="setting-selection {$this->required}">{$this->name}</label>
        <div id="{$this->name}">
            {$existingHtml}
        </div>
        <a id="{$this->name}keyAddText" href="#">Add</a>
        <script>
            $( document ).ready( function() {
                $( '#{$this->name}keyAddText' ).click( function( event ){
                    var text = $( '#{$this->name}_placeholderKeySettings' ).html();
                    $( this).prev().append( text );

                    registerEvents();
                    event.preventDefault();
                    return false;
                });

                registerEvents();
                function registerEvents()
                {
                    var elements = $( ".{$this->name}keyRemove" );
                    elements.unbind( 'click' );//To avoid having many click events bound to one
                    elements.click( function( event )
                    {
                        if( confirm( "Are you sure you want to remove this row?" ) )
                        {
                        $( this ).parent().remove();
                        }
                        event.preventDefault();
                        return false;
                    } );
                }
            });
        </script>
        </div>
HTML;
    }

    public function parseValue()
    {
        $builder = [ ];

        if ($this->value) {
            for ($i = 0, $end = sizeof( $this->value->keys ); $i < $end; $i++) {
                $builder[ $this->value->keys[ $i ] ] = $this->value->values[ $i ];
            }
        }

        return $builder;
    }

}