<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes;

use Rhubarb\Scaffolds\FeedImport\Helpers\FeedStringTools;

abstract class FeedSettingTypeInt extends FeedSettingType
{
    /**
     * Indicates that this setting requires a value
     *
     * @var bool
     */
    protected $required = true;

    /**
     * Override parent to cast boolean type on passed value
     *
     * @param string $value
     */
    public function __construct( $value = null )
    {
        parent::__construct( $value );

        // cast $value to an int when loading from the database
        $this->value = $this->ForceValueToInteger( $this->value );
    }

    /**
     * Gets a numeric input for this setting
     *
     * @return string HTML
     */
    public function getInput()
    {
        $required = "";
        if ($this->required) {
            $required = "required";
        }

        $title = FeedStringTools::UpperCamelCaseToWords( $this->name );

        return <<<HTML
            <div class="setting-selection-body">
                <label for="{$this->name}" class="setting-selection {$required}">{$title}:</label>
                <input type="number" id="{$this->name}" name="FeedSettings[{$this->name}]" value="{$this->value}" class="setting-selection {$required}" />
            </div>
HTML;
    }

    /**
     * Casts the value stored against this setting to an integer
     *
     * @return int
     */
    public function parseValue()
    {
        return $this->forceValueToInteger( $this->value );
    }

    /**
     * Ensures that only numbers exist in the passed value
     *
     * @param string|int $value
     *
     * @return int
     */
    protected function forceValueToInteger( $value )
    {
        if (is_int( $value )) {
            return $value;
        }

        return (int)preg_replace( "#[^[0-9]]#", "", $value );
    }

}