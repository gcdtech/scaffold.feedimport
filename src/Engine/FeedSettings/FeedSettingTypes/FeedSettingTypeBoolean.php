<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes;

use Rhubarb\Scaffolds\FeedImport\Helpers\FeedStringTools;

abstract class FeedSettingTypeBoolean extends FeedSettingType
{
    /**
     * Indicates that this setting requires a value
     * @var bool
     */
    protected $required = false;

    /**
     * The default value for this setting,
     * used if no value is supplied
     * @var bool
     */
    protected $default = false;

    /**
     * Override parent to cast boolean type on passed value
     *
     * @param string $value
     */
    public function __construct( $value = null )
    {
        parent::__construct( $value );

        // cast $value to a boolean for when loading from the database
        $this->value = (bool) $this->value;
    }

    /**
     * Gets a checkbox for this setting
     *
     * @return string HTML
     */
    public function getInput()
    {
        if( $this->value === true )
        {
            $checked = ' checked="checked"';
            $value = 1;
        }
        else
        {
            $checked = "";
            $value = 0;
        }

        $title = FeedStringTools::upperCamelCaseToWords( $this->name );

        // Two inputs are used as unchecked checkboxes do not submit a POST value
        // A second hidden input is used to save the actual true/false value of the setting
        return <<<HTML
            <div class="setting-selection-body">
                <label for="{$this->name}" class="setting-selection">{$title}:</label>
                <span class="setting-selection">
                    <input type="checkbox" id="{$this->name}" {$checked} class="setting-selection"/>
                    <input type="hidden" name="FeedSettings[{$this->name}]" value="{$value}" />
                </span>
            </div>
HTML;
    }

    /**
     * Casts the value stored against this setting to a boolean
     * @return bool
     */
    public function parseValue()
    {
        return (bool) $this->value;
    }
}