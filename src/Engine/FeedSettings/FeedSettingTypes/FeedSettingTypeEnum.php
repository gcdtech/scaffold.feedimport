<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes;

use Rhubarb\Scaffolds\FeedImport\Helpers\FeedStringTools;

abstract class FeedSettingTypeEnum extends FeedSettingType
{
    /**
     * Key value array of values (keys) and labels(values)
     * for this object
     *
     * @var array
     */
    protected $values = array();

    /**
     * Gets a text input for this setting
     *
     * @return string HTML
     */
    public function getInput()
    {
        $title = FeedStringTools::UpperCamelCaseToWords( $this->name );
		$required = ($this->required) ? "required" : "";

        $html = <<<HTML
            <div class="setting-selection-body">
                <label for="{$this->name}" class="setting-selection {$required}">{$title}:</label>
                <select name="FeedSettings[{$this->name}]" id="{$this->name}" class="setting-selection">
HTML;
        foreach ($this->values as $value => $label) {
            $selected = "";
            if ($value == $this->value) {
                $selected = ' selected="selected"';
            }
            $html .= <<<HTML
                    <option value="{$value}"{$selected}>{$label}</option>
HTML;
        }
        $html .= <<<HTML
                </select>
            </div>
HTML;

        return $html;
    }
}