<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes;

use Rhubarb\Scaffolds\FeedImport\Exceptions\FeedException;
use Rhubarb\Scaffolds\FeedImport\FeedImportModule;

/**
 * Class FeedSettingType
 *
 * @package Propertynews\Feeds\Feeds\FeedSettings\FeedSettingTypes
 */
abstract class FeedSettingType
{

    /**
     * The name of this setting
     *
     * @var string
     */
    protected $name = "";

    /**
     * Indicates that this setting requires a value
     *
     * @var bool
     */
    protected $required = true;

    /**
     * The value assigned to this setting
     *
     * @var mixed
     */
    protected $value;

    /**
     * The default value for this setting,
     * used if no value is supplied
     *
     * @var mixed
     */
    protected $default;

    /**
     * @var mixed Placeholder that is there as a helper for user inputting data
     */
    protected $placeholder;

    /**
     * Instantiates a setting object
     *
     * @abstract
     *
     * @param null $value The Value assigned to this setting. Set null to use $this->default
     */
    public function __construct( $value = null )
    {
        $this->name = preg_replace( "#^.*feedsetting#i", "", get_called_class() );

        if ($value === null) {
            $this->value = $this->default;
        } else {
            $this->value = $value;
        }
    }

    /**
     * Method stub, should be implemented for all feed setting types.
     *
     * @abstract
     * @return mixed
     */
    abstract function getInput();

    /**
     * Stub Method. This should be implemented when it
     * is required to manipulate a setting's value at run
     * time
     *
     * @return mixed
     */
    public function parseValue()
    {
        return $this->value;
    }

    /**
     * Gets the class name for the passed setting
     *
     * @static
     *
     * @param string $setting
     *
     * @throws FeedException If the setting has no corresponding class
     * @return string Class name
     */
    public static function getSettingClassName( $setting )
    {
        $setting = FeedImportModule::getClassByClassName( "FeedSetting", "FeedSetting$setting" );
        if (class_exists( $setting )) {
            return $setting;
        }

        throw new FeedException( "No settings class found for $setting" );
    }

    /**
     * Parses a key/val array as a settings array, calling appropriate parseValue()
     * methods on each settings object
     *
     * @static
     *
     * @param $settings
     *
     * @return array
     */
    public static function parseSettings( $settings )
    {
        $classSettings = array( "File", "Reader", "Parser", "Processor", "Reporter" );
        $parsedSettings = array();

        foreach ($settings as $settingName => $settingValue) {
            if (in_array( $settingName, $classSettings ) == false) {
                $className = self::getSettingClassName( $settingName );
                /**
                 * @var FeedSettingType $setting
                 */
                $setting = new $className( $settingValue );
                $parsedSettings[ $settingName ] = $setting->parseValue();
            } else {
                $parsedSettings[ $settingName ] = $settingValue;
            }
        }

        return $parsedSettings;
    }
}
