<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes\FeedSettingTypeText;

class FeedSettingFileURL extends FeedSettingTypeText
{

    protected $default = "";
    /**
     * Value to use as a placeholder for form inputs
     *
     * @var string
     */
    protected $placeholder = "http://www.example.com/data.csv";
}