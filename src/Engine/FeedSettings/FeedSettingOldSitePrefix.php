<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes\FeedSettingTypeText;

class FeedSettingOldSitePrefix extends FeedSettingTypeText
{
    /**
     * The placeholder for the input
     *
     * @var string
     */
    protected $placeholder = "FDXX-";

    /**
     * This setting is not compulsory
     *
     * @var bool
     */
    protected $required = false;

}