<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes\FeedSettingTypeBoolean;

class FeedSettingFTPPassiveMode extends FeedSettingTypeBoolean
{
    /**
     * The default value for this setting,
     * used if no value is supplied
     * @var bool
     */
    protected $default = false;
}