<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes\FeedSettingTypeText;

class FeedSettingFileName extends FeedSettingTypeText
{
    /**
     * The default value for this setting,
     * used if no value is supplied
     *
     * @var bool
     */
    protected $default = "data.csv";

    /**
     * Value to use as a placeholder for form inputs
     *
     * @var string
     */
    protected $placeholder = "data.csv";
}