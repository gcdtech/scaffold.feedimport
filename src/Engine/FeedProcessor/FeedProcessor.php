<?php

namespace Rhubarb\Scaffolds\FeedImport\Engine\FeedProcessor;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedControl;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedController;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedLogger;
use Rhubarb\Scaffolds\FeedImport\Exceptions\FeedException;

abstract class FeedProcessor extends FeedControl
{

    /**
     * Stores the object currently being processed
     *
     * @var object
     */
    protected $currentObject;

    /**
     * Indicates if the current record is new
     *
     * @var bool
     */
    protected $currentRecordIsNew = false;

    /**
     * Stores the parsed data for the record currently being processed
     *
     * @var array
     */
    protected $currentItemData = array();

    /**
     * Stores the current record ID
     *
     * @var string
     */
    protected $currentRecordID;

    /**
     * Array of default values for each record
     *
     * @var array
     */
    protected $defaults = array();

    /**
     * Fields which must be present for each data row
     *
     * @var array
     */
    protected $requiredFields = array();

    /**
     * Counters used for stats recording
     *
     * @var array
     */
    protected $counter = array(
        "Skipped"   => 0,
        "Updated"   => 0,
    );

    /**
     * Adds required settings
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param mixed $data
     */
    abstract function process( $data );

    /**
     * Loads the contents of a CSV into a returned array
     *
     * @param        $filePath
     * @param string $delimiter
     * @param string $enclosure
     * @param string $escape
     *
     * @return array
     * @throws FeedException If unable to load/read file contents
     */
    protected function loadCSVFileToArray( $filePath, $delimiter = ",", $enclosure = '"', $escape = '\\' )
    {
        if (file_exists( $filePath ) == false) {
            throw new FeedException( "File not found: '{$filePath}'", FeedLogger::ERROR_GCD );
        }

        $contents = file_get_contents( $filePath );

        if ($contents === false) {
            throw new FeedException( "Could not read contents of CSV file '{$filePath}'", FeedLogger::ERROR_GCD );
        }

        $csvArray = str_getcsv( $contents, $delimiter, $enclosure, $escape );

        return $csvArray;
    }

    /**
     * Reads in a csv file to a data array (first row must be headers).
     * This array can be used by the parser. This should NOT be used as the primary
     * method of retrieving record data, only for loading small amounts of additional
     * data for use in processing, eg an agent details file.
     *
     * @param        $filePath
     * @param array  $headerOverrides
     * @param null   $returnArrayKey
     * @param string $delimiter
     * @param string $enclosure
     * @param string $escape
     *
     * @return array
     * @throws FeedException
     */
    protected function loadCSVFileToArrayFirstRowHeaders(
        $filePath,
        $headerOverrides = array(),
        $returnArrayKey = null,
        $delimiter = ",",
        $enclosure = '"',
        $escape = '\\'
    ) {
        if (file_exists( $filePath ) == false) {
            throw new FeedException( "File not found: '{$filePath}'", FeedLogger::ERROR_GCD );
        }

        $contents = fopen( $filePath, "r" );

        if ($contents === false) {
            throw new FeedException( "Could not read contents of CSV file '{$filePath}'", FeedLogger::ERROR_GCD );
        }

        $csvArray = array();
        $headerRow = fgetcsv( $contents, 0, $delimiter, $enclosure, $escape );
        if ($headerRow != false) {
            $headers = array();

            foreach ($headerRow as $index => $header) {
                if (array_key_exists( $header, $headerOverrides )) {
                    $header = $headerOverrides[ $header ];
                }
                $headers[ $index ] = $header;
            }

            $headerCount = count( $headers );

            /** @noinspection PhpAssignmentInConditionInspection */
            while ($row = fgetcsv( $contents, 0, $delimiter )) {
                $rowParsed = array();

                foreach ($row as $index => $value) {
                    if ($index >= $headerCount) {
                        FeedController::$logger->LogEvent( FeedLogger::WARNING_GCD,
                            "Unmapped values in CSV file '{$filePath}'" );
                        continue;
                    } else {
                        $rowParsed[ $headers[ $index ] ] = $value;
                    }
                }

                if (
                    $returnArrayKey != null &&
                    in_array( $returnArrayKey, $headers ) &&
                    array_key_exists( $returnArrayKey, $rowParsed ) &&
                    array_key_exists( $rowParsed[ $returnArrayKey ], $csvArray ) == false
                ) {
                    $csvArray[ $rowParsed[ $returnArrayKey ] ] = $rowParsed;
                } else {
                    $csvArray[ ] = $rowParsed;
                }
            }

            if ($returnArrayKey != null) {
                ksort( $csvArray );
            }
        }

        return $csvArray;
    }

    /**
     * Returns the counter value for the counter specified in $type
     *
     * @param $type
     *
     * @return int
     */
    public function getCounter( $type )
    {
        return $this->counter[ $type ];
    }

    public function addCounter( $type )
    {
        $this->counter[ $type ]++;
    }

    /**
     * Handles the execution of counter actions once the feed has run
     */
    protected function handleCounters()
    {
        $counterLog = "";
        foreach ($this->counter as $counter => $count) {
            $counterLog .= "Rows {$counter}: {$count}\r\n";
        }
        FeedController::$logger->logEvent( FeedLogger::INFO, $counterLog );
    }

    /**
     * Checks if a field has been removed by the data tracker
     *
     * @param $field
     *
     * @return bool
     */
    protected function fieldRemovedByTracker( $field )
    {
        if (array_key_exists( "RemovedByTracker", $this->currentItemData ) && in_array( $field,
                $this->currentItemData[ "RemovedByTracker" ] )
        ) {
            return true;
        }

        return false;
    }

    /**
     * Loops through $this->requiredFields and checks that each has a value
     *
     * @throws FeedException
     */
    protected function checkRequiredFields()
    {
        // Check for required Fields
        foreach ($this->requiredFields as $requiredField) {
            if (
                (
                    array_key_exists( $requiredField, $this->currentItemData ) == false ||
                    empty( $this->currentItemData[ $requiredField ] )
                ) &&
                (
                    $this->fieldRemovedByTracker( $requiredField ) == false
                )
            ) {
                throw new FeedException( "The {$requiredField} field (required) is empty", FeedLogger::WARNING );
            }
        }
    }

    /**
     * Sets currentItemData values to a default if no value is set
     */
    protected function setDefaultData()
    {
        if (is_array( $this->defaults )) {
            foreach ($this->defaults as $key => $value) {
                if (array_key_exists( $key,
                        $this->currentItemData ) == false || $this->currentItemData[ $key ] == false
                ) {
                    $this->currentItemData[ $key ] = $this->defaults[ $key ];
                }
            }
        }
    }
}