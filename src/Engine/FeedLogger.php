<?php
/**
 * @author Scott
 * Date: 30/06/15
 */

namespace Rhubarb\Scaffolds\FeedImport\Engine;

use Rhubarb\Scaffolds\FeedImport\BackgroundTasks\FeedRunTask;
use Rhubarb\Scaffolds\FeedImport\Model\Feed;
use Rhubarb\Scaffolds\FeedImport\Model\FeedLog;

/**
 * Class FeedLogger
 *
 * @package Rhubarb\Scaffolds\FeedImport\Engine
 */
class FeedLogger extends FeedControl
{

    /**
     * Normal event code, only GCD to be notified
     */
    const INFO_GCD = 0;

    /**
     * Normal event code
     */
    const INFO = 1;

    /**
     * Warning error event code, only report to GCD
     */
    const WARNING_GCD = 2;

    /**
     * Warning error event code
     */
    const WARNING = 3;

    /**
     * Critical error code, only report to GCD
     */
    const ERROR_GCD = 4;

    /**
     * Critical error code
     */
    const ERROR = 5;

    /**
     * No data
     */
    const ERROR_NO_DATA = 6;

    protected static $parseHtmlReplacements = array(
        "\n" => '<br />',
        "\t" => '&nbsp;&nbsp;&nbsp;&nbsp;'
    );

    /**
     * Tracks the highest level of log entry from the current execution
     *
     * @var int
     */
    public $highestLogCode = 0;

    /** @var FeedLog */
    public $dbLog;

    /**
     * tracks if the log has been handled or not
     *
     * @var bool
     */
    public $handledLogs = false;

    /**
     * Tracks the ID of the feed currently executing
     *
     * @var string
     */
    protected $feedId;

    /**
     * Used to store file name including full path to the log file to be written to
     *
     * @var string
     */
    protected $logFile;

    /**
     * Stores logged events
     *
     * @var array
     */
    private $logItems = array();

    /**
     * Keeps a track of the number of events logged
     *
     * @var int
     */
    public $logItemsCount = 0;

    function __construct()
    {
        $idTag = FeedController::$settings[ "IDTag" ];
        $dateString = date( 'Y-m-d_H-i-s' );

        $path = APPLICATION_ROOT_DIR . "/feeds/{$idTag}/logs";
        if (!is_dir( $path )) {
            mkdir( $path, 0777, true );
        }
        $this->logFile = $path . "/{$idTag}-{$dateString}.log.gz";
        $this->feedId = FeedController::$feed->FeedID;
        $this->dbLog = new FeedLog();
        $this->dbLog->FeedID = FeedController::$feed->FeedID;
        $this->dbLog->LogFile = $this->logFile;
        $this->dbLog->save();

        // make an initial temp log so that a failed feed still has a file to display
        file_put_contents($this->logFile, gzcompress("Log file is empty", 9));
    }

    /**
     * @param $code
     * @param $message
     */
    public function logEvent( $code, $message )
    {
        if ($code > $this->highestLogCode) {
            $this->highestLogCode = $code;
        }

        $this->logItems[ $this->logItemsCount ] = array(
            "Code"    => $code,
            "Message" => $message
        );

        $this->logItemsCount++;
        
        if(getenv('FEEDS_CLI') === false) {
            FeedRunTask::logMessage(FeedController::$feed->TaskStatusID,
                $this->translateLogCode($code) . " - {$message}");
        }
    }

    /**
     * Allows other methods/objects to read all events currently logged
     *
     * @return array Log Items for the current execution
     */
    public function getLogItems()
    {
        return $this->logItems;
    }

    /**
     * Converts a log message into html, according to replacement specified in self::$parseHtmlReplacements
     *
     * @static
     *
     * @param string $message The message to be parsed
     *
     * @return string HTML representation of $message
     */
    public static function parseMessageAsHTML( $message )
    {
        $message = str_replace( array_keys( self::$parseHtmlReplacements ),
            array_values( self::$parseHtmlReplacements ), $message );

        return $message;
    }

    /**
     * Returns a readable translation of a passed log code number
     *
     * @static
     *
     * @param int $code Log code to be translated
     *
     * @return string Translated log code
     */
    public static function translateLogCode( $code )
    {
        switch ($code) {
            case self::INFO_GCD:
                return "GCD Info";
            case self::INFO:
                return "Info";
            case self::WARNING_GCD:
                return "GCD Warning";
            case self::WARNING:
                return "Warning";
            case self::ERROR_GCD:
                return "GCD Error";
            case self::ERROR:
                return "Error";
            case self::ERROR_NO_DATA:
                return "No Data";
            default:
                return "Unmapped log code ( {$code} )";
        }
    }

    public static function getStatusClass( $id )
    {
        switch ($id) {
            default:
            case self::INFO_GCD:
            case self::INFO:
                return 'ok';
            case self::WARNING_GCD:
            case self::WARNING:
                return 'warning';
            case self::ERROR_GCD:
            case self::ERROR:
            case self::ERROR_NO_DATA:
                return 'error';
        }
    }

    public function handleLogs()
    {
        $this->handledLogs = true;
        FeedController::$logger->logEvent(FeedLogger::INFO_GCD, "Writing log file - " . date("Y-m-d H:i:s"));
        file_put_contents( $this->logFile, gzcompress( serialize( $this->logItems ), 9 ) );

        switch ($this->highestLogCode) {
            default:
            case 0:
            case 1:
                $status = Feed::LAST_RUN_STATE_COMPLETED;
                break;
            case 2:
            case 3:
                $status = Feed::LAST_RUN_STATE_COMPLETED_WITH_WARNINGS;
                break;
            case 4:
            case 5:
            case 6:
                $status = Feed::LAST_RUN_STATE_ERROR;
                break;
        }

        $this->dbLog->Updated = FeedController::$processor->getCounter( 'Updated' );
        $this->dbLog->Skipped = FeedController::$processor->getCounter( 'Skipped' );

        if (intval($this->dbLog->Updated) + intval($this->dbLog->Skipped) === 0) {
            if ($status === Feed::LAST_RUN_STATE_COMPLETED) {
                $status = Feed::LAST_RUN_STATE_COMPLETED_WITH_NO_CHANGES;
            }
        }

        $this->dbLog->Status = $status;
        $this->dbLog->save();
    }
}