<?php

namespace Rhubarb\Scaffolds\FeedImport\Helpers;

use Rhubarb\Crown\String\StringTools;

class FeedStringTools extends StringTools
{
    /**
     * Converts an upper camel case string to readable,
     * space separated words
     *
     * @static
     *
     * @param $string
     *
     * @return string
     */
    public static function upperCamelCaseToWords( $string )
    {
        // Explode out words by upper case followed by multiple lower case,
        // then implode the resultant array using a space
        return implode( " ",
            preg_split( "#([A-Z][a-z]+)#", $string, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE ) );
    }
}