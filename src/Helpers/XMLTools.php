<?php

namespace Rhubarb\Scaffolds\FeedImport\Helpers;

/**
 * Class XMLTools
 *
 * @package Propertynews\Feeds\Helpers
 */
class XMLTools
{
    /**
     * @param      $xml
     * @param bool $includeAttributes
     *
     * @return array
     */
    public static function deserializeXmlToArray( $xml, $includeAttributes = false )
    {
        $reader = new \XMLReader();
        $reader->XML( $xml );
        if ($includeAttributes) {
            $array = XMLTools::fancyReadXmlToArray( $reader );
        } else {
            $array = XMLTools::readXmlToArray( $reader );
        }
        $reader->close();

        unset( $reader );

        return $array;
    }

    /**
     * @param \XMLReader $xml
     *
     * @return array
     */
    private static function fancyReadXmlToArray( $xml )
    {
        $array = array();

        while ($xml->read()) {
            switch ($xml->nodeType) {
                case \XMLReader::END_ELEMENT:
                    return $array;

                case \XMLReader::ELEMENT:
                    $node = array();

                    $elementName = $xml->name;

                    if ($xml->hasAttributes) {
                        $attributes = array();
                        while ($xml->moveToNextAttribute()) {
                            $attributes[ $xml->name ] = $xml->value;
                        }
                        $node[ "attributes" ] = $attributes;
                    }

                    if (!$xml->isEmptyElement) {
                        $children = self::FancyReadXmlToArray( $xml );
                        if (!is_array( $children )) {
                            $node[ "value" ] = $children;
                        } else {
                            $node[ "children" ] = $children;
                        }
                    }

                    $array[ $elementName ] = $node;

                    break;

                case \XMLReader::TEXT:
                    $array = $xml->value;
            }
        }

        return $array;
    }

    /**
     * @param \XMLReader $reader
     * @param string     $initialElement
     *
     * @return array
     */
    private static function readXmlToArray( $reader, $initialElement = "" )
    {
        $array = array();
        $firstElement = true;

        $currentNodeName = $initialElement;
        $currentNodeValue = "";

        while ($reader->read()) {
            switch ($reader->nodeType) {
                case \XMLReader::END_ELEMENT:

                    if ($currentNodeName == "") {
                        return $array;
                    }

                    if (isset( $array[ $currentNodeName ] )) {
                        if (is_array( $array[ $currentNodeName ] )) {
                            if ($firstElement) {
                                $temp = $array[ $currentNodeName ];
                                unset( $array[ $currentNodeName ] );
                                $array[ $currentNodeName ][ ] = $temp;
                                $array[ $currentNodeName ][ ] = $currentNodeValue;
                                $firstElement = false;
                            } else {
                                $array[ $currentNodeName ][ ] = $currentNodeValue;
                                $firstElement = false;
                            }
                        } else {
                            $array[ $currentNodeName ] = array( $array[ $currentNodeName ], $currentNodeValue );
                            $firstElement = false;
                        }
                    } else {
                        $array[ $currentNodeName ] = $currentNodeValue;
                    }

                    $currentNodeName = "";
                    $currentNodeValue = "";

                    break;
                case \XMLReader::ELEMENT:

                    if ($currentNodeName == "") {
                        $currentNodeName = $reader->name;
                    } else {
                        $currentNodeValue = XMLTools::readXmlToArray( $reader, $reader->name );

                        if (isset( $array[ $currentNodeName ] )) {
                            if (is_array( $array[ $currentNodeName ] )) {
                                if ($firstElement) {
                                    $temp = $array[ $currentNodeName ];
                                    unset( $array[ $currentNodeName ] );
                                    $array[ $currentNodeName ][ ] = $temp;
                                    $array[ $currentNodeName ][ ] = $currentNodeValue;
                                    $firstElement = false;
                                } else {
                                    $array[ $currentNodeName ][ ] = $currentNodeValue;
                                    $firstElement = false;
                                }
                            } else {
                                $array[ $currentNodeName ] = array( $array[ $currentNodeName ], $currentNodeValue );
                                $firstElement = false;
                            }
                        } else {
                            $array[ $currentNodeName ] = $currentNodeValue;
                        }

                        $currentNodeName = "";
                    }
                    break;
                case \XMLReader::TEXT:
                case \XMLReader::CDATA:
                    $currentNodeValue = $reader->value;
                    break;
            }
        }

        return $array;
    }
}