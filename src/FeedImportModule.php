<?php

namespace Rhubarb\Scaffolds\FeedImport;

use Rhubarb\Crown\Module;
use Rhubarb\Leaf\Crud\UrlHandlers\CrudUrlHandler;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedFile\FeedFileFTP;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedFile\FeedFileLocal;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedFile\FeedFileLocalZip;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedFile\FeedFilePost;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedFile\FeedFileURL;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedFile\FeedFileURLSecure;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedFile\FeedFileURLWithPost;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingBadWords;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingBaseUrl;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingDataFileInZip;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingEnableDataTracking;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingFileAlwaysRequired;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingFileName;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingFileURL;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingFTPFile;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingFTPPassiveMode;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingFTPServer;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingIsIncremental;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingOldSitePrefix;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingPostDataJSON;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingRemotePassword;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingRemoteUsername;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingSchedule;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingZipFile;
use Rhubarb\Scaffolds\FeedImport\Leaves\Feeds\Log\LogItem;
use Rhubarb\Stem\Schema\SolutionSchema;

class FeedImportModule extends Module
{
    public static $overridesPath;
    public static $classes = [
        'FeedFile'      => [],
        'FeedParser'    => [],
        'FeedProcessor' => [],
        'FeedReporter'  => [],
        'FeedSetting'   => [],
        'FeedReader'    => [],
    ];

    /**
     * @param $path String Overrides path that is used for Project specific properties
     */
    public function __construct($path)
    {
        parent::__construct();

        self::$overridesPath = $path;
    }

    /**
     * @param $class String|Array path to a custom File class
     */
    public static function addFileClass($class)
    {
        self::addClass('File', $class);
    }

    /**
     * @param $class String|Array class to a custom Parser class
     */
    public static function addParserClass($class)
    {
        self::addClass('Parser', $class);
    }

    /**
     * @param $class String|Array class to a custom Processor class
     */
    public static function addProcessorClass($class)
    {
        self::addClass('Processor', $class);
    }

    /**
     * @param $class String|Array class to a custom Reporter class
     */
    public static function addReporterClass($class)
    {
        self::addClass('Reporter', $class);
    }

    /**
     * @param $class String|Array class to a custom Setting class
     */
    public static function addSettingClass($class)
    {
        self::addClass('Setting', $class);
    }

    /**
     * @param $class String | Array class to a custom Setting class
     */
    public static function addReaderClass($class)
    {
        self::addClass('Reader', $class);
    }

    /**
     * @param $class String|Array class to a custom File class
     */
    public static function removeFileClass($class)
    {
        self::removeClass('File', $class);
    }

    /**
     * @param $class String|Array class to a custom Parser class
     */
    public static function removeParserClass($class)
    {
        self::removeClass('Parser', $class);
    }

    /**
     * @param $class String|Array class to a custom Processor class
     */
    public static function removeProcessorClass($class)
    {
        self::removeClass('Processor', $class);
    }

    /**
     * @param $class String|Array class to a custom Reporter class
     */
    public static function removeReporterClass($class)
    {
        self::removeClass('Reporter', $class);
    }

    /**
     * @param $class String|Array class to a custom Setting class
     */
    public static function removeSettingClass($class)
    {
        self::removeClass('Setting', $class);
    }

    /**
     * @param $class String|Array class to a custom Reader class
     */
    public static function removeReaderClass($class)
    {
        self::removeClass('Reader', $class);
    }

    private static function removeClass($type, $class)
    {
        $type = "Feed$type";
        if (!is_array($class)) {
            $class = [$class];
        }

        foreach ($class as $c) {
            if (isset( self::$classes[ $type ] )) {
                $key = array_search($c, self::$classes);

                if ($key) {
                    unset( self::$classes[ $key ] );
                }
            }
        }
    }

    private static function addClass($type, $class)
    {
        $type = "Feed$type";

        if (!is_array($class)) {
            $class = [$class];
        }

        foreach ($class as $c) {
            if (isset( self::$classes[ $type ] )) {
                self::$classes[ $type ][] = $c;
            }
        }
    }

    /**
     * @param $type String
     * @param $name String
     *
     * @returns String|Bool full namespace + class name of the class so it is ready to be
     *                 instantiated. If it doesn't exist returns false
     */
    public static function getClassByClassName($type, $name)
    {
        foreach (self::$classes[ $type ] as $class) {
            $pos = strrpos($class, '\\');
            $className = substr($class, $pos + 1);

            if ($name == $className) {
                return $class;
            }
        }

        return false;
    }

    /**
     *
     * Assumes that all override objects are going to be placed in their IDTag folder with the type as the prefix and
     * idTag as the suffix of the name.
     *
     * @param $idTag String
     * @param $type  String
     *
     * @return string
     */
    public static function getOverrideNamespace($idTag, $type)
    {
        return self::$overridesPath . '\\' . $idTag . '\\' . $type . $idTag;
    }

    protected function registerUrlHandlers()
    {
        parent::registerUrlHandlers();

        $this->addUrlHandlers(
            [
                "/" => new CrudUrlHandler( 'Feed', 'Rhubarb\Scaffolds\FeedImport\Leaves\Feeds', [ ],
                    [
                        'log-item/' => new CrudUrlHandler( 'FeedLog', 'Rhubarb\Scaffolds\FeedImport\Leaves\Feeds\Log' )
                    ]
                )
            ]
        );
    }

    protected function initialise()
    {
        parent::initialise();

        SolutionSchema::registerSchema('Feed', 'Rhubarb\Scaffolds\FeedImport\Model\FeedSolutionSchema');

        self::addFileClass(
            [
                FeedFileFTP::class,
                FeedFileLocal::class,
                FeedFileLocalZip::class,
                FeedFilePost::class,
                FeedFileURL::class,
                FeedFileURLSecure::class,
                FeedFileURLWithPost::class
            ]
        );

        self::addSettingClass(
            [
                FeedSettingBadWords::class,
                FeedSettingBaseURL::class,
                FeedSettingEnableDataTracking::class,
                FeedSettingFileAlwaysRequired::class,
                FeedSettingFileName::class,
                FeedSettingFileURL::class,
                FeedSettingFTPFile::class,
                FeedSettingFTPPassiveMode::class,
                FeedSettingFTPServer::class,
                FeedSettingIsIncremental::class,
                FeedSettingOldSitePrefix::class,
                FeedSettingPostDataJSON::class,
                FeedSettingRemotePassword::class,
                FeedSettingRemoteUsername::class,
                FeedSettingSchedule::class,
                FeedSettingZipFile::class,
                FeedSettingDataFileInZip::class
            ]
        );
    }

    /**
     * Applies a specified namespace to each string in an array
     */
    public static function applyNamespaceToArray($strings, $namespace)
    {
        for ($i = 0, $end = sizeof($strings); $i < $end; $i++) {
            $strings[ $i ] = $namespace . $strings[ $i ];
        }

        return $strings;
    }
}