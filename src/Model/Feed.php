<?php

namespace Rhubarb\Scaffolds\FeedImport\Model;

use FeedSettings;
use Rhubarb\Scaffolds\BackgroundTasks\Models\BackgroundTaskStatus;
use Rhubarb\Scaffolds\FeedImport\Email\FeedEmail;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedControl;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedLogger;
use Rhubarb\Scaffolds\FeedImport\Exceptions\FeedException;
use Rhubarb\Scaffolds\FeedImport\FeedImportModule;
use Rhubarb\Stem\Collections\Collection;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\LessThan;
use Rhubarb\Stem\Filters\OrGroup;
use Rhubarb\Stem\Models\Model;
use Rhubarb\Stem\Repositories\MySql\Schema\Columns\MySqlEnumColumn;
use Rhubarb\Stem\Schema\Columns\AutoIncrementColumn;
use Rhubarb\Stem\Schema\Columns\BooleanColumn;
use Rhubarb\Stem\Schema\Columns\DateTimeColumn;
use Rhubarb\Stem\Schema\Columns\IntegerColumn;
use Rhubarb\Stem\Schema\Columns\JsonColumn;
use Rhubarb\Stem\Schema\Columns\StringColumn;
use Rhubarb\Stem\Schema\ModelSchema;

/**
 * Class Feed
 *
 * @package Propertynews\Feeds\FeedClasses
 * @property int $FeedID Repository field
 * @property string $FeedName Repository field
 * @property string $IDTag Repository field
 * @property string $FeedSettings Repository field
 * @property string $DataMappings Repository field
 * @property string $Reports Repository field
 * @property string $Status Repository field
 * @property DateTime $NextScheduledRun Repository field
 * @property Boolean $Disabled Repository field
 * @property DateTime $LastStartTime Repository field
 * @property int $RetryCount Repository field
 * @property int $TaskStatusID Repository field
 * @property DateTime $TaskStartTime Repository field
 * @property boolean $ReRunRequired Repository field
 * @property boolean $SeparateRunner Repository field
 * @property FeedLog[]|Collection $Logs Repository field
 * @property \stdClass $DataValues Repository field
 * @property string $LastRunState Repository field
 * @property int $LastRunCreatedCount Repository field
 * @property int $LastRunSkippedCount Repository field
 * @property-write int $ProcessInfo set the TaskStatusID of the feed (intended for passing through the BackgroundTaskID) {@link setProcessInfo()}
 * @property-write mixed $NextRunTime Sets when this feed will next be run by the dispatcher {@link setNextRunTime()}
 * @property-read FeedLog $NewLogFile {@link getNewLogFile()}
 * @property-read mixed $ControlObject
 * @property-read string $SelectClassOfType Gets select input for a feed class of a specified type
 */
class Feed extends Model
{
    const LAST_RUN_STATE_COMPLETED = "Completed";
    const LAST_RUN_STATE_COMPLETED_WITH_WARNINGS = "Warnings";
    const LAST_RUN_STATE_COMPLETED_WITH_NO_CHANGES = "No changes in last feed";
    const LAST_RUN_STATE_ERROR = "Error";

    const LAST_RUN_STATES = [
        self::LAST_RUN_STATE_COMPLETED,
        self::LAST_RUN_STATE_COMPLETED_WITH_WARNINGS,
        self::LAST_RUN_STATE_COMPLETED_WITH_NO_CHANGES,
        self::LAST_RUN_STATE_ERROR
    ];

    /**
     * Returns the schema for this data object.
     *
     * @return \Rhubarb\Stem\Schema\ModelSchema
     */
    protected function createSchema()
    {
        $schema = new ModelSchema("tblFeed");
        $schema->addColumn(
            new AutoIncrementColumn("FeedID"),
            new StringColumn("FeedName", 100),
            new StringColumn("IDTag", 100),
            new JsonColumn("FeedSettings"),
            new JsonColumn("DataMappings"),
            new JsonColumn("Reports"),
            new MySqlEnumColumn("Status", "Ready", self::FEED_STATUS_ARRAY),
            new MySqlEnumColumn("LastRunState", self::LAST_RUN_STATE_COMPLETED_WITH_NO_CHANGES, self::LAST_RUN_STATES),
            new DateTimeColumn("NextScheduledRun"),
            new BooleanColumn("Disabled"),
            new DateTimeColumn("LastStartTime"),
            new IntegerColumn("RetryCount"),
            new IntegerColumn("TaskStatusID"),
            new DateTimeColumn("TaskStartTime"),
            new JsonColumn("DataValues"),
            new BooleanColumn("ReRunRequired"),
            new BooleanColumn("SeparateRunner"),
            new IntegerColumn("LastRunCreatedCount"),
            new IntegerColumn("LastRunSkippedCount")
        );

        $schema->labelColumnName = "FeedName";

        return $schema;
    }

    protected $packedFields = array();

    public static $feedLog;

    /**
     * @return mixed
     */
    public static function GetRunningFeedCount()
    {
        $feeds = self::GetRunningFeeds();

        return count($feeds);
    }

    /**
     * Returns Ready feeds in greatest need of a good running
     * Includes pending feeds to ensure that the number of Pending feeds doesn't simply spiral
     *
     * @param int $limit
     *
     * @param bool $separateRunner
     * @return Feed[]|Collection
     */
    public static function GetReadyToRunFeeds($limit = 5, $separateRunner = false)
    {
        $feeds = self::find(
            new AndGroup([
                new OrGroup([
                    new Equals("Status", "Ready"),
                    new Equals("Status", "Pending")
                ]),
                new Equals("Disabled", 0),
                new LessThan("NextScheduledRun", date('Y-m-d H:i:s')),
                new Equals("SeparateRunner", (int)$separateRunner),
            ])
        );
        $feeds->setRange(0, $limit);
        $feeds->replaceSort(array("NextScheduledRun" => true));

        return $feeds;
    }

    /**
     * Returns the Pending feed in greatest need of a good running
     *
     * @param bool $separateRunner
     * @return bool|Feed
     */
    public static function GetMostUrgentPendingFeed($separateRunner = false)
    {
        $feeds = self::find(
            new AndGroup([
                new Equals("Status", "Pending"),
                new Equals("Disabled", 0),
                new LessThan("NextScheduledRun", date('Y-m-d H:i:s')),
                new Equals("SeparateRunner", (int)$separateRunner),
            ])
        );
        $feeds->replaceSort(array("NextScheduledRun" => true));

        if (count($feeds) > 0) {
            return $feeds[0];
        }

        return false;
    }

    /**
     * returns feeds with status of "Running"
     *
     * @param bool $separateRunner
     * @return Collection|static[]
     */
    public static function GetRunningFeeds($separateRunner = false)
    {
        return self::find(
            new AndGroup([
                new Equals("Status", "Running"),
                new Equals("SeparateRunner", (int)$separateRunner),
            ])
        );
    }

    const FEED_STATUS_ARRAY = ["Ready", "Pending", "Running", "Locked"];



    public function getDataValues()
    {
        return json_decode($this->modelData["DataValues"]);
    }

    /**
     * @return FeedLog
     */
    public function getNewLogFile()
    {
        $log = new FeedLog();
        $log->FeedID = $this->FeedID;
        $log->Status = $this->Status;

        return $log;
    }

    public function getFeedSettings()
    {
        if (isset($this->modelData['FeedSettings'])) {
            return (array)$this->modelData['FeedSettings'];
        }

        return [];
    }

    public function getDataMappings()
    {
        if (isset($this->modelData['DataMappings'])) {
            return (array)$this->modelData['DataMappings'];
        }

        return [];
    }

    /**
     * @param $feedId
     *
     * @return Feed
     * @throws FeedException
     */
    public static function loadReadyOrPendingFeed($feedId)
    {
        $feed = new Feed($feedId);

        if ($feed->Disabled) {
            throw new FeedException("Feed #" . $feedId . " is disabled");
        }

        if ($feed->Status != "Ready" && $feed->Status != "Pending") {
            $message = "Feed #" . $feedId . " is not in a ready/pending state. Current state: " . $feed->Status;
            throw new FeedException($message);
        }

        return $feed;
    }

    /**
     * @return FeedControl Control class
     */
    public function getControlObject($controlClassType)
    {
        // Attempt to find classes in this order
        $controlClassNames[] = FeedImportModule::getOverrideNamespace($this->IDTag, "Feed{$controlClassType}");

        if (array_key_exists($controlClassType,
                $this->FeedSettings) && $this->FeedSettings[$controlClassType] != ""
        ) {
            $controlClassNames[] = FeedImportModule::getClassByClassName("Feed{$controlClassType}",
                $this->FeedSettings[$controlClassType]);
        }

        $controlClassName = $this->loadClassFromArray($controlClassNames);

        return new $controlClassName();
    }

    /**
     * Loops through an array of class names until an existing class
     * is found
     *
     * @param array $classNames Ordered array of class names
     * @param string $namespace
     *
     * @return FeedControl implementation
     * @throws FeedException Thrown if no class can be found
     */
    protected function loadClassFromArray($classNames, $namespace = "")
    {
        foreach ($classNames as $class) {
            if (class_exists("$namespace$class")) {
                return "$namespace$class";
            }
        }

        throw new FeedException("Class not found $namespace" . end($classNames) . "", FeedLogger::ERROR_GCD);
    }

    /**
     * Clears process info
     */
    public function clearProcessInfo()
    {
        $this->TaskStatusID = 0;
        $this->TaskStartTime = 0;
    }

    /**
     * set the TaskStatusID of the feed
     * (intended for passing through the BackgroundTaskID)
     *
     * @param int $taskId
     * @param string $taskStartTime
     *
     */
    public function setProcessInfo($taskId, $taskStartTime)
    {
        $this->TaskStatusID = $taskId;
        $this->TaskStartTime = $taskStartTime;
    }

    /**
     * Sets when this feed will next be run by the dispatcher
     */
    public function setNextRunTime()
    {
        $nextRunTime = "3000-01-01 00:00:00";
        if (array_key_exists("Schedule",
                $this->FeedSettings) && $this->FeedSettings["Schedule"] != "" && $this->FeedSettings["Schedule"] != "Not enabled"
        ) {
            $nextRunTime = self::convertTimeToUnix($this->FeedSettings["Schedule"], $this->TaskStartTime);
        }

        if ($this->ReRunRequired) {
            $nextRunTime = new \DateTime();
            $this->Status = 'Pending';
            $this->ReRunRequired = false;
        }

        $this->NextScheduledRun = $nextRunTime;
        $this->Save();
    }

    private function convertTimeToUnix($time, $startTime)
    {
        $next = 0;
        switch ($time) {
            case "+15 minutes":
                $next = 900;
                break;
            case "+30 minutes":
                $next = 1800;
                break;
            case "+1 hour":
                $next = 3600;
                break;
            case "+6 hours":
                $next = 21600;
                break;
            case strstr($time, 'Custom'):
                $time = str_replace('Custom ', '', $time);
                $next = $time * 3600;
                break;
            default:
                if (strpos($time, 'Daily') !== false) {
                    $time = str_replace('Daily ', '', $time);
                    $time = str_split($time, 2);
                    $seconds = $time[0] * 60 * 60 + $time[1] * 60;
                    $date = new \DateTime('tomorrow');

                    return $date->add(new \DateInterval('PT' . $seconds . 'S'));
                }
        }

        return $startTime->add(new \DateInterval('PT' . $next . 'S'));
    }

    /**
     * Returns Next Scheduled Run in a nice format
     *
     * @return string
     */
    public function getNextScheduledRun()
    {
        if (isset($this->modelData['NextScheduledRun'])) {
            return $this->modelData['NextScheduledRun']->format("d-M-Y H:i");
        }

        return "";
    }

    /**
     * Scans directories of implementable classes for classes of the specified type
     *
     * @param string $type The Type to scan for eg FeedFile
     *
     * @return array Class names matching the passed type
     */
    protected function getClassesOfType($type)
    {
        $finalClasses = [];
        foreach (FeedImportModule::$classes[$type] as $path) {
            $replacements = array(
                ".php"
            );

            $pos = strrpos($path, '\\');
            $className = substr($path, $pos + 1);

            $finalClasses[] = str_replace($replacements, '', $className);
        }

        return $finalClasses;
    }

    /**
     * Gets select input for a feed class of a specified type
     *
     * @param string $type class of a specified type eg File
     *
     * @return string HTML select input
     */
    public function getSelectClassOfType($type)
    {
        // Check for an override class for this feed
        $idTag = $this->IDTag;
        if (class_exists(FeedImportModule::getOverrideNamespace($idTag, 'Feed' . $type))) {
            // If an override class is found, return a disabled select box - overrides cannot be overridden via settings
            return <<<HTML
				<label class="readonly">Feed {$type}:</label>
				<select disabled="disabled" name="FeedSettings[{$type}]" class="readonly">
					<option value="{$idTag}">{$idTag}</option>
				</select>
HTML;
        } else {
            //If there is no override class, just get a select box of all available inputs of this type
            $classes = $this->getClassesOfType("Feed{$type}");
            $output = <<<HTML
				<label>Feed {$type}:</label>
				<select name="FeedSettings[{$type}]" id="{$type}">
HTML;
            foreach ($classes as $class) {
                $selected = "";
                if (array_key_exists($type, $this->FeedSettings) && $this->FeedSettings[$type] == $class) {
                    $selected = ' selected="selected"';
                }
                $displayClass = $class;
                if ($displayClass == "") {
                    $displayClass = "Default";
                }
                $output .= <<<HTML
					<!--suppress HtmlUnknownAttribute -->
					<option value="{$class}" {$selected}>{$displayClass}</option>
HTML;
            }
            $output .= <<<HTML
				</select>
HTML;

            return $output;
        }
    }

    /**
     * Returns the passed string in the correct format for use as an IDTag
     *
     * @param string $idTag
     *
     * @return string
     */
    public static function formatIDTag($idTag)
    {
        $idTag = ucwords(trim($idTag));

        return preg_replace("#[^[:alnum:]]#", "", $idTag);
    }

    /**
     * Used to determine if the passed IDTag is already in use
     *
     * @static
     *
     * @param string $idTag IDTag (should be passed through FormatIDTag to ensure format is correct)
     *
     * @return bool FALSE if the IDTag is used by an existing Feed, FALSE otherwise
     */
    public static function idTagInUse($idTag)
    {
        if (empty($idTag) == false && $idTag == self::formatIDTag($idTag)) {
            $feeds = self::find();
            foreach ($feeds as $feed) {
                if ($feed->IDTag == $idTag) {
                    return true;
                }
            }

            return false;
        } else {
            return true;
        }
    }

    /**
     * @throws \Exception
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     */
    public function disableAndAlert()
    {
        $this->Status = 'Ready';
        $this->save();

        $settings = FeedSettings::singleton();
        $emailContent = <<<TEXT
Feed disabled

The {$this->FeedName} feed did not run after {$this->RetryCount} attempts. It has automatically been disabled. Once it has been fixed, it can be retried.
TEXT;
        $email = new FeedEmail(
            "A Propertynews Feed has been disabled",
            $emailContent
        );
        $email->addRecipient($settings->errorEmail);
        $email->setSender($settings->senderEmail, "Feeds System");
        $email->send();
    }

    public function allowFeedToRetry()
    {
        $this->Status = 'Ready';
        $this->RetryCount++;
        $this->SetNextRunTime();
        $this->save();
    }

    /**
     * determines if the feed's background process is still runnning or has fallen over
     *
     * @return bool
     */
    public function stillProcessing()
    {
        $bgTaskStatus = new BackgroundTaskStatus($this->TaskStatusID);
        // first check if the background task is still running or not
        if ($bgTaskStatus->TaskStatus == BackgroundTaskStatus::TASK_STATUS_RUNNING) {
            // if the background task is still running (process exists) then leave it alone
            if (file_exists("/proc/" . $bgTaskStatus->ProcessID)) {
                return true;
            }
        }
        return false;
    }
}
