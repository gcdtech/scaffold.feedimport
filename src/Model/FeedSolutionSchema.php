<?php

namespace Rhubarb\Scaffolds\FeedImport\Model;

use Rhubarb\Scaffolds\FeedImport\BackgroundTasks\FeedBackgroundTaskStatus;
use Rhubarb\Stem\Schema\SolutionSchema;

/**
 * Class FeedSolutionSchema
 *
 * @package Propertynews\Feeds\Model
 */
class FeedSolutionSchema extends SolutionSchema
{
    function __construct()
    {
        parent::__construct( 0.15 );

        $this->addModel( 'Feed',    __NAMESPACE__ . '\Feed' );
        $this->addModel( 'FeedLog', __NAMESPACE__ . '\FeedLog' );
        $this->addModel( 'FeedBackgroundTaskStatus', FeedBackgroundTaskStatus::class );
    }

    protected function defineRelationships()
    {
        parent::defineRelationships();

        $this->declareOneToManyRelationships([
            'Feed' => [
                'Logs' => 'FeedLog.FeedID'
            ]
        ]);
    }
    
}
