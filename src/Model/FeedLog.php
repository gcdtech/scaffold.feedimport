<?php

namespace Rhubarb\Scaffolds\FeedImport\Model;

use Rhubarb\Scaffolds\FeedImport\Engine\FeedController;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedLogger;
use Rhubarb\Stem\Collections\Collection;
use Rhubarb\Stem\Exceptions\RecordNotFoundException;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Models\Model;
use Rhubarb\Stem\Repositories\MySql\MySql;
use Rhubarb\Stem\Schema\Columns\AutoIncrementColumn;
use Rhubarb\Stem\Schema\Columns\IntegerColumn;
use Rhubarb\Stem\Schema\Columns\StringColumn;
use Rhubarb\Stem\Schema\ModelSchema;

/**
 * Class FeedLog
 *
 * @package Propertynews\Feeds\Model
 * @property int    $FeedLogID
 * @property int    $FeedID
 * @property string $Status
 * @property string $CompletedTime
 * @property string $LogFile
 * @property int    $Updated
 * @property int    $Skipped
 * @property-read Feed $Feed Relationship
 * @property-read mixed $FileEncodedUrl {@link getFileEncodedUrl()}
 */
class FeedLog extends Model
{

    /**
     * A singleton cache array of database table fields
     *
     * @var array
     */
    public static $fieldsCache = array();

    /**
     * A singleton cache array for the enum values of the Status field
     *
     * @var array
     */
    public static $statusValues = array();

    /**
     * Fields not to treat as counts
     *
     * @var array
     */
    public static $nonCountFields = array(
        "FeedLogID",
        "FeedID",
        "Status",
        "CompletedTime",
        "LogFile"
    );

    /**
     * Returns the schema for this data object.
     *
     * @return \Rhubarb\Stem\Schema\ModelSchema
     */
    protected function createSchema()
    {
        $schema = new ModelSchema( 'tblFeedLog' );

        $schema->addColumn(
            new AutoIncrementColumn( 'FeedLogID' ),
            new IntegerColumn( 'FeedID' ),
            new StringColumn( 'Status', 200 ),
            new StringColumn ( 'CompletedTime', 200 ),
            new StringColumn ( 'LogFile', 200 ),
            new IntegerColumn( 'Updated' ),
            new IntegerColumn( 'Skipped' )
        );

        return $schema;
    }

    /**
     * Loads the most recent log record from the database for the passed feed ID
     *
     * @static
     *
     * @param int $feedId
     *
     * @return bool|FeedLog
     */
    public static function LoadMostRecentLogForFeed( $feedId )
    {
        try {
            $log = FeedLog::findLast( new Equals( 'FeedID', $feedId ) );
        } catch ( RecordNotFoundException $ex ) {
            return false;
        }

        return $log;
    }

    /**
     * Loads a ClassifiedFeedLog object using the passed ID
     *
     * @static
     *
     * @param int $logId
     *
     * @return FeedLog
     */
    public static function LoadByLogID( $logId )
    {
        try {
            $log = new FeedLog( $logId );
        } catch ( RecordNotFoundException $ex ) {
            $log = false;
        }

        return $log;
    }

    /**
     * Gets the feed ID for the passed log ID
     *
     * @static
     *
     * @param int $feedLogId
     *
     * @return bool|int
     */
    public static function GetFeedIDFromFeedLogID( $feedLogId )
    {
        try {
            $log = new FeedLog( $feedLogId );
            $id = $log->FeedID;
        } catch ( RecordNotFoundException $ex ) {
            $id = false;
        }

        return $id;
    }

    /**
     * Gets a number of feed log records from the database for the
     * passed feed id, up to the optionally specified limit
     *
     * @static
     *
     * @param int $feedId Feed ID who's logs are required
     * @param int $limit  The maximum number of logs to return
     *
     * @return Collection|FeedLog[]
     */
    public static function GetRecentLogsForFeed( $feedId, $limit = 50 )
    {
        return FeedLog::find( new Equals( 'FeedID', $feedId ) )->setRange( 0, $limit );
    }

    /**
     * Log housekeeping - removes old logs
     *
     * @static
     *
     * @param int $logsToKeep
     *
     * @return int Number of logs removed
     */
    public static function DoHouseKeeping( $logsToKeep = 50 )
    {
        $feedId = FeedController::$feed->FeedID;
        // number starts at 0, so subtract one
        $logsToKeep--;

        // Get all logs older than number the specified number to keep (limited to 100)
        $logsToRemove = <<<SQL
			SELECT
				FeedLogID
			FROM
				tblFeedLog
			WHERE
				FeedID = "{$feedId}" AND
				LogFile != ""
			ORDER BY
				CompletedTime DESC
			LIMIT {$logsToKeep}, 100
SQL;

        $logsToRemove = MySql::executeStatement( $logsToRemove );
        $removedCount = 0;
        if (empty( $logsToRemove ) == false) {
            foreach ($logsToRemove as $logToRemove) {
                $log = FeedLog::LoadByLogID( $logToRemove );
                if ($log->RemoveLogFile()) {
                    $removedCount++;
                }
            }
        }

        return $removedCount;
    }

    /**
     * Removes the log file associated with this record
     *
     * @return bool
     */
    public function RemoveLogFile()
    {
        if (file_exists( $this->LogFile )) {
            @unlink( $this->LogFile );
            $this->LogFile = "";
            $this->Save();

            return true;
        }

        return false;
    }

    public function getFileEncodedUrl()
    {
        return urlencode( base64_encode( $this->LogFile ) );
    }

}