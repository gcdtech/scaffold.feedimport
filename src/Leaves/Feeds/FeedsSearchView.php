<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Rhubarb\Leaf\SearchPanel\Leaves\SearchPanelView;

class FeedsSearchView extends SearchPanelView
{
    protected function createSubLeaves()
    {
        $controls = $this->model->searchControls;

        $this->registerSubLeaf(...$controls);
    }

    protected function printViewContent()
    {
        print '<div class="c-filters"><div class="c-filter c-filter--1"></div>';
        foreach ($this->model->searchControls as $control) {
            print '<div class="c-filter">' . $control->getLabel() . $control . '</div>';
        }
        print '</div>';
    }

}
