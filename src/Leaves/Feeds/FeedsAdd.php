<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Rhubarb\Crown\Exceptions\ForceResponseException;
use Rhubarb\Crown\Response\RedirectResponse;
use Rhubarb\Leaf\Crud\Leaves\CrudLeaf;
use Rhubarb\Patterns\Mvp\Crud\ModelForm\ModelFormPresenter;
use Rhubarb\Scaffolds\FeedImport\Model\Feed;

class FeedsAdd extends CrudLeaf
{
    /**
     * @var FeedsAddModel
     */
    protected $model;

    protected function getViewClass()
    {
        return FeedsAddView::class;
    }

    protected function createModel()
    {
        return new FeedsAddModel();
    }

    protected function onModelCreated()
    {
        parent::onModelCreated();

        $this->model->verifyPressedEvent->attachHandler(function ()
        {
            return $this->verify();
        });
    }

    protected function verify()
    {
        if ($this->model->restModel->FeedName != "") {
            $idTag = Feed::formatIDTag( $this->model->restModel->FeedName );

            if (Feed::idTagInUse( $idTag )) {
                return false;
            } else {
                $this->model->restModel->IDTag = $idTag;
                $this->model->restModel->save();

                return "../" . $this->model->restModel->uniqueIdentifier . "/class-settings/";
            }
        }
    }
}