<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds\Log;

use Rhubarb\Leaf\Crud\Leaves\ModelBoundLeaf;
use Rhubarb\Leaf\Crud\Leaves\ModelBoundModel;
use Rhubarb\Leaf\Leaves\LeafModel;

class LogItem extends ModelBoundLeaf
{
    protected function getViewClass()
    {
        return LogItemView::class;
    }

    /**
     * Should return a class that derives from LeafModel
     *
     * @return LeafModel
     */
    protected function createModel()
    {
        return new ModelBoundModel();
    }
}
