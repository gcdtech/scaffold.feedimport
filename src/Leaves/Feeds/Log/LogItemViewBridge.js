var bridge = function (presenterPath) {
    window.rhubarb.viewBridgeClasses.ViewBridge.apply(this, arguments);
};

bridge.prototype = new window.rhubarb.viewBridgeClasses.ViewBridge();
bridge.prototype.constructor = bridge;

bridge.prototype.attachEvents = function () {
    $(".ok").hide();

    $(".hideInfo").click(
        function () {
            $(".ok").toggle(!this.checked);
        }
    );

    $(".hideWarning").click(
        function () {
            $(".warning").toggle(!this.checked);
        }
    );

    $(".hideError").click(
        function () {
            $(".error").toggle(!this.checked);
        }
    );

    window.rhubarb.viewBridgeClasses.ViewBridge.prototype.attachEvents.call(this);
};

window.rhubarb.viewBridgeClasses.LogItemViewBridge = bridge;
