<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds\Log;

use Rhubarb\Leaf\Crud\Leaves\CrudView;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedLogger;
use Rhubarb\Scaffolds\FeedImport\Model\FeedLog;
use Rhubarb\Leaf\Leaves\LeafDeploymentPackage;

class LogItemView extends CrudView
{
    protected function printViewContent()
    {
        /** @var FeedLog $feedLog */
        $feedLog = $this->model->restModel;

        if (file_exists($feedLog->LogFile)) {
            ?>
            <div>
                <a href="/<?= $feedLog->FeedID ?>/log/" class="c-button u-marg-left">Back to logs</a>
            </div>
            <div class="u-clear-both u-marg-bottom"></div>
            <div class="u-pad">
                <div class="c-filters">
                    <div class="c-filter">
                        Hide Info Logs
                        <input class="hideInfo" type="checkbox" checked>
                    </div>
                    <div class="c-filter">
                        Hide Warning Logs
                        <input class="hideWarning" type="checkbox">
                    </div>
                    <div class="c-filter">
                        Hide Error Logs
                        <input class="hideError" type="checkbox">
                    </div>
                </div>
            </div>
            <div>
                <table class="c-table c-table--app">
                    <?php
                    $feedfile = unserialize(gzuncompress(file_get_contents($feedLog->LogFile)));
                    foreach ($feedfile as $line) {
                        $code = FeedLogger::getStatusClass($line['Code']);
                        $styleTag = "";

                        if ($code === "ok") {
                            $styleTag = "style=\"display: none\"";
                        }
                        ?>
                        <tr class="data-row <?= $code ?>" <?= $styleTag ?>>
                            <td colspan="0">
                                <b><?= FeedLogger::translateLogCode($line['Code']) ?></b>
                            </td>
                            <td colspan="6">
                                <?= $line['Message'] ?>
                            </td>
                        </tr>
                        <?php
                        $line['Message'];
                    }
                    ?>
                </table>
            </div>
            <?php
        } else {
            print 'Log file not found - ' . $feedLog->LogFile;
        }
    }

    protected function getViewBridgeName()
    {
        return "LogItemViewBridge";
    }

    public function getDeploymentPackage()
    {
        return new LeafDeploymentPackage(__DIR__ . "/LogItemViewBridge.js");
    }
}
