<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Rhubarb\Crown\Exceptions\ForceResponseException;
use Rhubarb\Crown\Response\RedirectResponse;
use Rhubarb\Leaf\Crud\Leaves\CrudLeaf;
use Rhubarb\Stem\Models\Model;

class FeedsSettings extends CrudLeaf
{
    public $redirect;

    /** @var FeedsSettingsModel $model */
    protected $model;

    protected function getViewClass()
    {
        return FeedsSettingsView::class;
    }

    public function setRestModel(Model $restModel)
    {
        if ($restModel->FeedSettings && 
            array_key_exists("Schedule", $restModel->FeedSettings) && 
            $restModel->FeedSettings["Schedule"] != "Not enabled"
        ) {
            $this->model->EnableScheduling = true;
            $this->model->Schedule = $restModel->FeedSettings["Schedule"];
        }

        parent::setRestModel($restModel);
    }

    protected function saveRestModel()
    {
        $this->redirect = true;

        if (is_array($_POST[ "FeedSettings" ])) {
            $this->model->restModel->FeedSettings = array_merge($this->model->restModel->FeedSettings, $_POST["FeedSettings"]);
        }

        $this->saveSchedule();

        if ($this->redirect) {
            return parent::saveRestModel();
        }
    }

    protected function saveSchedule()
    {
        $settings = $this->model->restModel->FeedSettings;

        $settings["Schedule"] = "Not enabled";
        if ($this->model->EnableScheduling){
            $settings["Schedule"] = $this->model->Schedule;
        }

        $this->model->restModel->FeedSettings = $settings;
        $this->model->restModel->setNextRunTime();
    }

    protected function redirectAfterSave()
    {
        throw new ForceResponseException(new RedirectResponse('/'));
    }

    protected function redirectAfterCancel()
    {
        $this->redirectAfterSave();
    }
}
