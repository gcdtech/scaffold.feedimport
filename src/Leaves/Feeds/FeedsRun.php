<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Rhubarb\Crown\Context;
use Rhubarb\Crown\Request\WebRequest;
use Rhubarb\Leaf\Crud\Leaves\ModelBoundLeaf;
use Rhubarb\Leaf\Leaves\Forms\Form;
use Rhubarb\Leaf\Leaves\LeafModel;
use Rhubarb\Patterns\Mvp\Crud\ModelForm\ModelFormPresenter;
use Rhubarb\Scaffolds\FeedImport\BackgroundTasks\FeedRunTask;
use Rhubarb\Stem\Models\Model;

class FeedsRun extends ModelBoundLeaf
{
    protected $feedTask;

    /**
     * @var FeedsRunModel
     */
    protected $model;

    protected function getViewClass()
    {
        return FeedsRunView::class;
    }

    protected function parseRequest( WebRequest $request )
    {
        parent::parseRequest( $request );

        if( !isset( $this->model->BackgroundTaskStatusID ) )
        {
            $this->feedTask = FeedRunTask::initiate( array( 'FeedID' => $this->model->restModel->FeedID ) );
            $this->model->BackgroundTaskStatusID = $this->feedTask->BackgroundTaskStatusID;
            $this->initialiseView();
        }
    }

    /**
     * Should return a class that derives from LeafModel
     *
     * @return LeafModel
     */
    protected function createModel()
    {
        return new FeedsRunModel();
    }
}