<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Rhubarb\Crown\Exceptions\ForceResponseException;
use Rhubarb\Crown\Response\RedirectResponse;
use Rhubarb\Leaf\Crud\Leaves\CrudLeaf;
use Rhubarb\Leaf\Leaves\Forms\Form;
use Rhubarb\Leaf\Leaves\HtmlPresenter;
use Rhubarb\Leaf\Leaves\Leaf;
use Rhubarb\Leaf\Leaves\LeafModel;

class FeedsCollection extends CrudLeaf
{
    protected function getViewClass()
    {
        return FeedsCollectionView::class;
    }
}