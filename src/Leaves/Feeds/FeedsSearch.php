<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Rhubarb\Leaf\Controls\Common\Checkbox\Checkbox;
use Rhubarb\Leaf\Controls\Common\SelectionControls\DropDown\DropDown;
use Rhubarb\Leaf\Controls\Common\Text\TextBox;
use Rhubarb\Leaf\SearchPanel\Leaves\SearchPanel;
use Rhubarb\Scaffolds\FeedImport\Model\Feed;
use Rhubarb\Stem\Filters\Contains;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\Group;
use Rhubarb\Stem\Filters\JsonContains;

class FeedsSearch extends SearchPanel
{
    public function __construct($name = "")
    {
        parent::__construct($name);

        $this->model->autoSubmit = true;
    }

    protected function createSearchControls()
    {
        $feedName = new TextBox('FeedName');
        $feedSettings = new TextBox('FeedSettings');

        $statuses = Feed::FEED_STATUS_ARRAY;
        array_unshift($statuses, 'All');

        $status = new DropDown('FeedStatus');
        $status->setSelectionItems($statuses);

        $lastRunClass = new DropDown('State');
        $lastRunClass->setSelectionItems(array_merge(['All'], Feed::LAST_RUN_STATES));

        $edge = new Checkbox('Edge');
        $edge->setLabel('Toggle Edge Feeds');

        $incDisabled = new Checkbox('Disabled');
        $incDisabled->setLabel('Show Disabled');

        return [
            $feedName,
            $feedSettings,
            $status,
            $lastRunClass,
            $edge,
            $incDisabled
        ];
    }

    public function populateFilterGroup(Group $filterGroup)
    {
        parent::populateFilterGroup($filterGroup);

        if ($name = $this->model->getSearchValue('FeedName')) {
            $filterGroup->addFilters(new Contains('FeedName', $name));
        }

        if ($ref = $this->model->getSearchValue('FeedSettings')) {
            $filterGroup->addFilters(new JsonContains('FeedSettings', $ref));
        }

        if ($status = $this->model->getSearchValue('FeedStatus')) {
            if ($status !== 'All') {
                $filterGroup->addFilters(new Equals('Status', $status));
            }
        }

        if ($lastRunClass = $this->model->getSearchValue('State')) {
            if ($lastRunClass !== 'All') {
                $filterGroup->addFilters(new Equals('LastRunState', $lastRunClass));
            }
        }

        if ($this->model->getSearchValue('Edge')) {
            $filterGroup->addFilters(new Equals('SeparateRunner', 1));
        } else {
            $filterGroup->addFilters(new Equals('SeparateRunner', 0));
        }

        if (!$this->model->getSearchValue('Disabled')) {
            $filterGroup->addFilters(new Equals('Disabled', 0));
        }
    }

    /**
     * @return string
     */
    protected function getViewClass()
    {
        return FeedsSearchView::class;
    }
}
