<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Rhubarb\Leaf\Crud\Leaves\ModelBoundModel;

class FeedsRunModel extends ModelBoundModel
{
	public $feedTaskId;
	public $BackgroundTaskStatusID;

	protected function getExposableModelProperties()
	{
		$list = parent::getExposableModelProperties();
		$list[] = "BackgroundTaskStatusID";

		return $list;
	}
}