<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Rhubarb\Leaf\Crud\Leaves\CrudModel;

class FeedsSettingsModel extends CrudModel
{
    public $EnableScheduling;
    public $Schedule = "Not enabled";
}
