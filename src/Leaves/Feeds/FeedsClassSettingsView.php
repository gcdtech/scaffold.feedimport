<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Rhubarb\Leaf\Crud\Leaves\CrudView;
use Rhubarb\Scaffolds\FeedImport\Model\Feed;

class FeedsClassSettingsView extends CrudView
{
    protected function printViewContent()
    {
        parent::printViewContent();

        $saveButton = $this->leaves[ 'Save' ];
        $saveButton->addHtmlAttribute('class', 'c-button c-button--primary');
        $model = $this->model->restModel;
        if ($model != "" && $model->FeedID) {
            $feed = new Feed($model->FeedID);
        } else {
            throw new \Exception('No feed associated');
        }
        print '<h2>'. $this->model->restModel->FeedName .'</h2>
                <div class="o-wrap o-wrap--narrow u-pad">
                    <div class="c-form u-pad-x2 u-fill-white u-bordered">
                        <div class="c-form__item">
                            '.$feed->getSelectClassOfType("File").'
                        </div>
                        <div class="c-form__item">
                            '.$feed->getSelectClassOfType("Reader").'
                        </div>
                        <div class="c-form__item">
                            '.$feed->getSelectClassOfType("Parser").'
                        </div>
                        <div class="c-form__item">
                            '.$feed->getSelectClassOfType("Processor").'
                        </div>
                        <div class="c-form__item">
                            '.$feed->getSelectClassOfType("Reporter").'
                        </div>
                        <div class="c-form__actions">
                            '.$this->leaves[ 'Save' ].'
                        </div>
                    </div>
                </div>
                ';
    }
}
