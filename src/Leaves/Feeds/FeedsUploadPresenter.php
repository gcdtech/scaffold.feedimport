<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Rhubarb\Crown\Layout\LayoutModule;
use Rhubarb\Patterns\Mvp\Crud\ModelForm\ModelFormPresenter;
use Rhubarb\Scaffolds\FeedImport\Engine\UploadHandler;
use Rhubarb\Scaffolds\FeedImport\Engine\UploadHandler\UploadHandlerProvider;
use Rhubarb\Scaffolds\FeedImport\Model\Feed;

class FeedsUploadPresenter extends ModelFormPresenter
{

    protected function beforeRenderView()
    {
        LayoutModule::disableLayout();
    }

    protected function getViewClass()
    {
        /** @var Feed $feed */
        $feed = $this->getModel();

        /** @var FeedFile|UploadHandlerProvider $fileHandler */
        $fileHandler = $feed->getControlObject('File');

        if (is_a($fileHandler, UploadHandlerProvider::class)) {
            return $fileHandler->getUploadHandler( $feed );
        } else {
            die( 'Uploader unavailable' );
        }
    }

}