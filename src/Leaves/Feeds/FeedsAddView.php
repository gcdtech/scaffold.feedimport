<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Rhubarb\Leaf\Controls\Common\Buttons\Button;
use Rhubarb\Leaf\Controls\Common\Text\TextBox;
use Rhubarb\Leaf\Crud\Leaves\CrudView;
use Rhubarb\Leaf\Leaves\LeafDeploymentPackage;

class FeedsAddView extends CrudView
{
    protected function getViewBridgeName()
    {
        return "FeedsAddViewBridge";
    }

    public function getDeploymentPackage()
    {
        return new LeafDeploymentPackage(__DIR__ . "/FeedsAddViewBridge.js");
    }

    public function createSubLeaves()
    {
        parent::createSubLeaves();

        $this->registerSubLeaf(
            new TextBox( "FeedName" ),
            new TextBox( "IDTag" ),
            new Button( "Verify", "Verify" )
        );
        $this->leaves[ "IDTag" ]->setPlaceholderText( "Verify Feed Name to fill in this field" );
        $feedName = $this->leaves["FeedName"];
        $idTag = $this->leaves["IDTag"];
        $verifyButton = $this->leaves["Verify"];
        $cancelButton = $this->leaves["Cancel"];
        $feedName->addHtmlAttribute("class","c-text-field");
        $idTag->addHtmlAttribute("class","c-text-field");
        $verifyButton->addHtmlAttribute("class","c-button c-button--primary");
        $cancelButton->addHtmlAttribute("class","c-button");
    }

    protected function printViewContent()
    {
        parent::printViewContent();

        print '<div class="o-wrap o-wrap--narrow u-pad"><div class="u-pad-x2 u-bordered u-fill-white c-form">
                    <div class="c-form__item">
                    <label for="FeedsAdd_FeedName" class="c-label">Feed Name</label>
                    '.$this->leaves["FeedName"].'
                    </div>
                    <div class="c-form__item">
                    <label for="FeedsAdd_IDTag" class="c-label">ID Tag</label>
                    '.$this->leaves["IDTag"].'
                    </div>
                    <div class="c-form__actions">
                    '.$this->leaves["Verify"] . $this->leaves[ 'Cancel' ].'
                    </div>
                </div></div>';
    }
}