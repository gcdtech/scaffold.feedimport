var bridge = function (presenterPath) {
	window.rhubarb.viewBridgeClasses.ViewBridge.apply(this, arguments);
};

bridge.prototype = new window.rhubarb.viewBridgeClasses.ViewBridge();
bridge.prototype.constructor = bridge;

bridge.prototype.attachEvents = function () {
	var self = this;

	$( "#FeedsAdd_IDTag, #FeedsEdit_IDTag" ).attr( 'disabled', 'disabled' );

	$("#FeedsAdd_Verify").click(function (event) {
		self.raiseServerEvent("verifyPressed", function (result) {
			if (!result) {
				alert("Sorry, this feed name corresponds to an existing IDTag");
			}
			else {
				window.location.href = result;
			}
		});

		event.preventDefault();
		return false;
	});

	window.rhubarb.viewBridgeClasses.ViewBridge.prototype.attachEvents.call(this);
};

window.rhubarb.viewBridgeClasses.FeedsAddViewBridge = bridge;
