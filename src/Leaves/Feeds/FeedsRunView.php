<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Propertynews\Feeds\Settings\FeedPageSettings;
use Rhubarb\Crown\Settings\HtmlPageSettings;
use Rhubarb\Leaf\Crud\Leaves\CrudView;
use Rhubarb\Scaffolds\FeedImport\Leaves\BackgroundTasks\FeedsBackgroundTaskProgress;

class FeedsRunView extends CrudView
{
    /**
     * @var FeedsRunModel
     */
    protected $model;

    public $feedTaskId = null;

    public function createSubLeaves()
    {
        $this->registerSubLeaf(
                new FeedsBackgroundTaskProgress( "FeedProgress" ) );

        $this->leaves[ "FeedProgress" ]->setBackgroundTaskStatusId( $this->model->BackgroundTaskStatusID );
    }

    protected function printViewContent()
    {
        print '<pre>';
        $htmlPageSettings = FeedPageSettings::singleton();
        $htmlPageSettings->pageTitle = 'Running ' . $this->model->restModel->FeedName;

        print $this->leaves[ "FeedProgress" ];
        print '</pre>';
    }
}