<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Propertynews\Feeds\Settings\FeedPageSettings;
use Rhubarb\Crown\Settings\HtmlPageSettings;
use Rhubarb\Leaf\Controls\Common\Checkbox\Checkbox;
use Rhubarb\Leaf\Crud\Leaves\CrudView;
use Rhubarb\Leaf\Leaves\LeafDeploymentPackage;
use Rhubarb\Leaf\Table\Leaves\Table;
use Rhubarb\Leaf\Table\Leaves\Columns\ClosureColumn;
use Rhubarb\Scaffolds\FeedImport\Model\Feed;
use Rhubarb\Stem\Filters\Equals;

class FeedsCollectionView extends CrudView
{
    public function createSubLeaves()
    {
        parent::createSubLeaves();

        $table = new Table(Feed::all());

        $this->registerSubLeaf(
            $searchPanel = new FeedsSearch('FeedsSearch')
        );

        $table->bindEventsWith($searchPanel);

        $lastRunClass = new ClosureColumn('State', function(Feed $feed) {
            switch ($feed->LastRunState) {
                case Feed::LAST_RUN_STATE_COMPLETED_WITH_WARNINGS:
                    return "<strong style='color: #f0b100'>⚠</strong>";
                case Feed::LAST_RUN_STATE_ERROR:
                    return "<strong style='color: red'>✖</strong>";
                case Feed::LAST_RUN_STATE_COMPLETED:
                    return "<strong style='color: green'>✔</strong>";
                case Feed::LAST_RUN_STATE_COMPLETED_WITH_NO_CHANGES:
                    return "<strong style='color: blue'>Ø</strong>";
                default:
                    return "<i style='color: grey'> $feed->LastRunClass </i>";
            }
        });

        $lastStartTime = new ClosureColumn('Last Run', function(Feed $feed) {
            return $feed->LastStartTime->format('dS M \a\t H:i');
        });

        $nextScheduledRun = new ClosureColumn('Next Run', function(Feed $feed) {
            return $feed->NextScheduledRun->format('dS M \a\t H:i');
        });

        $table->columns = [
            "FeedID",
            "FeedName",
            "Feed Status" => "Status",
            $lastRunClass,
            "LastRunCreatedCount",
            "LastRunSkippedCount",
            $lastStartTime,
            $nextScheduledRun,
            "Actions"     => <<<HTML
                <a href='/{FeedID}/run/'>Run feed</a> •
                <a href='/{FeedID}/log/'>History</a> •
                <a href='/{FeedID}/settings/'>Settings</a>
HTML
        ];

        $table->getRowCssClassesEvent->attachHandler(function ($model, $rowNumber) {
            /** @var Feed $model */
            $classes = ['data-row'];
            $classes[] = $model->LastRunState;

            return $classes;
        });

        $this->registerSubLeaf(
            new Checkbox("ShowDisabled"),
            $table
        );
    }

    protected function printViewContent()
    {
        parent::printViewContent();

        print "<div class='u-pad'>{$this->leaves['FeedsSearch']}</div>";

        $htmlPageSettings = FeedPageSettings::singleton();
        $htmlPageSettings->pageTitle = "Propertynews Feeds";
        $htmlPageSettings->headerButtons = '<a href="/add/" class="c-button c-button--primary">Add New Feed</a>';

        print $this->leaves[ 'Table' ];
    }

    protected function getViewBridgeName()
    {
        return "FeedsCollectionViewBridge";
    }

    public function getDeploymentPackage()
    {
        return new LeafDeploymentPackage(__DIR__ . "/FeedsCollectionViewBridge.js");
    }
}
