<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Propertynews\Feeds\Settings\FeedPageSettings;
use Rhubarb\Leaf\Controls\Common\Checkbox\Checkbox;
use Rhubarb\Leaf\Crud\Leaves\CrudView;
use Rhubarb\Scaffolds\FeedImport\Controllers\KeyValueControllers\DataMappingKeyValue;
use Rhubarb\Scaffolds\FeedImport\Controllers\KeyValueControllers\ReportsKeyValue;
use Rhubarb\Scaffolds\FeedImport\Controllers\ScheduleTimeDropDown;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedSettings\FeedSettingTypes\FeedSettingType;
use Rhubarb\Scaffolds\FeedImport\Exceptions\FeedException;
use Rhubarb\Scaffolds\FeedImport\Model\Feed;

class FeedsSettingsView extends CrudView
{
    public function createSubLeaves()
    {
        parent::createSubLeaves();

        $this->registerSubLeaf(
            new Checkbox("Disabled"),
            new Checkbox("EnableScheduling"),
            new Checkbox("SuppressNoDataErrors"),
            new ScheduleTimeDropDown("Schedule"),
            new DataMappingKeyValue('DataMappings'),
            new ReportsKeyValue("Reports")
        );

        $saveButton = $this->leaves['Save'];
        $saveButton->addHtmlAttribute('class', 'c-button c-button--primary');

        $cancelButton = $this->leaves['Cancel'];
        $cancelButton->addHtmlAttribute('class', 'c-button');
    }

    protected function printViewContent()
    {
        parent::printViewContent();

        $model = $this->model->restModel;

        $pageSettings = FeedPageSettings::singleton();
        $pageSettings->headerButtons = "<a href='/{$model->FeedID}/class-settings/' class='c-button c-button--primary'>Edit Classes</a>";

        $settings = $this->getFeedSettingsAsInputs($model);

        ?>
        <h2><?= $this->model->restModel->FeedName ?></h2>
            <div class="o-wrap o-wrap--narrow u-pad">
                <div class="c-form u-pad-x2 u-fill-white u-bordered">
                    <div class="c-form__item">
                        <label for="" class="c-label c-label--check"><?= $this->leaves['Disabled'] ?> Disabled</label>
                    </div>
                    <div class="c-form__item">
                        <label for="" class="c-label c-label--check"><?= $this->leaves['EnableScheduling'] ?> Enable Scheduling</label>
                    </div>
                    <div class="c-form__item">
                        <label for="" class="c-label c-label--check"><?= $this->leaves['SuppressNoDataErrors'] ?> Suppress no data errors</label>
                    </div>
                    <div class="c-form__item setting-selection-body">
                        <label for="" class="c-label">Schedule</label>
                        <?= $this->leaves['Schedule'] ?>
                    </div>
                    <div class="c-form__item setting-selection-body">
                        <label for="" class="c-label">DataMappings</label>
                        <?= $this->leaves['DataMappings'] ?>
                    </div>
                    <div class="c-form__item setting-selection-body">
                        <label for="" class="c-label">Reports</label>
                        <?= $this->leaves['Reports'] ?>
                    </div>
                    <div class="u-pad u-fill-shade u-marg-top">
                        <?php $this->layoutItemsWithContainer("", $settings); ?>
                    </div>
                    <div class="c-form__actions">
                        <?= $this->leaves[ 'Save' ] . $this->leaves[ 'Cancel' ] ?>
                    </div>
                </div>
            </div>
        <?php
    }

    /**
     * @param Feed $feed
     *
     * @return string
     * @throws FeedException
     */
    protected function getFeedSettingsAsInputs($feed)
    {
        $fileSettings = $feed->getControlObject('File')->getRequiredSettings();
        $parserSettings = $feed->getControlObject('Parser')->getRequiredSettings();
        $readerSettings = $feed->getControlObject('Reader')->getRequiredSettings();
        $processorSettings = $feed->getControlObject('Processor')->getRequiredSettings();
        $reporterSettings = $feed->getControlObject('Reporter')->getRequiredSettings();

        $settings = array_merge($fileSettings, $parserSettings, $processorSettings, $reporterSettings, $readerSettings);

        $output = "";
        if (!empty($settings)) {
            $output .= "<h3>Settings</h3>";
            foreach ($settings as $settingName) {
                $class = FeedSettingType::GetSettingClassName($settingName);

                if (array_key_exists($settingName, $feed->FeedSettings)) {
                    /* @var FeedSettingType $settingHtml */
                    $settingHtml = new $class($feed->FeedSettings[$settingName]);
                } else {
                    /* @var FeedSettingType $settingHtml */
                    $settingHtml = new $class();
                }

                $output .= '<div class="c-form__item">';
                $output .= $settingHtml->GetInput();
                $output .= '</div>';
            }
        }

        return $output;
    }
}
