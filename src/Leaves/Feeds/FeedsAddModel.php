<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Rhubarb\Crown\Events\Event;
use Rhubarb\Leaf\Crud\Leaves\CrudModel;

class FeedsAddModel extends CrudModel
{
	public $verifyPressedEvent;

	public function __construct()
	{
		parent::__construct();

		$this->verifyPressedEvent = new Event();
	}
}