<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Rhubarb\Leaf\Crud\Leaves\CrudView;
use Rhubarb\Leaf\Table\Leaves\Columns\ClosureColumn;
use Rhubarb\Leaf\Table\Leaves\Table;
use Rhubarb\Scaffolds\FeedImport\Model\FeedLog;
use Rhubarb\Stem\Filters\Equals;

class FeedsLogView extends CrudView
{
    public function createSubLeaves()
    {
        parent::createSubLeaves();

        $logs = FeedLog::find(
            new Equals('FeedID', $this->model->restModel->FeedID)
        )->addSort('FeedLogID', false);

        $table = new Table($logs, 10, 'LogTable');

        $completedTime = new ClosureColumn('Elapsed Time', function (FeedLog $log) {
            if ($log->CompletedTime) {
                return gmdate('H\h i\m s\s', $log->CompletedTime);
            }
        });

        $table->columns = [
            'Status',
            $completedTime,
            'LogFile',
            'Updated',
            'Skipped',
            '' => '<a href="../log-item/{FeedLogID}/">View</a>'
        ];

        $this->registerSubLeaf(
            $table
        );
    }

    protected function printViewContent()
    {
        $pageTitle = '<h2>Logs for ' . $this->model->restModel->FeedName . '</h2>';

        print $pageTitle . $this->leaves["LogTable"];
    }
}
