<?php

namespace Rhubarb\Scaffolds\FeedImport\Leaves\Feeds;

use Rhubarb\Crown\Exceptions\ForceResponseException;
use Rhubarb\Crown\Response\RedirectResponse;
use Rhubarb\Leaf\Crud\Leaves\CrudLeaf;
use Rhubarb\Patterns\Mvp\Crud\ModelForm\ModelFormPresenter;

class FeedsClassSettings extends CrudLeaf
{
    protected function getViewClass()
    {
        return FeedsClassSettingsView::class;
    }

    protected function saveRestModel()
    {
        if ($_POST[ "FeedSettings" ]) {
            foreach ($_POST[ "FeedSettings" ] as $key => $value) {
                if ($value == "") {
                    $_POST[ "FeedSettings" ][ $key ] = "Default";
                }
            }

            if ($this->model->restModel->FeedSettings) {
                $this->model->restModel->FeedSettings = array_merge( $this->model->restModel->FeedSettings, $_POST[ "FeedSettings" ] );
            } else {
                $this->model->restModel->FeedSettings = $_POST[ "FeedSettings" ];
            }
        }

        return parent::saveRestModel();
    }

    protected function redirectAfterSave()
    {
        throw new ForceResponseException( new RedirectResponse( '../settings/' ) );
    }
}