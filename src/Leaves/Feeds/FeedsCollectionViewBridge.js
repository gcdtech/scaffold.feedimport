var bridge = function (presenterPath) {
    window.rhubarb.viewBridgeClasses.ViewBridge.apply(this, arguments);
};

bridge.prototype = new window.rhubarb.viewBridgeClasses.ViewBridge();
bridge.prototype.constructor = bridge;

bridge.prototype.attachEvents = function () {
    $(".disabled").hide();

    $("#FeedsCollectionPresenter_ShowDisabled").click(
        function () {
            $(".disabled").toggle(this.checked);
        }
    );

    window.rhubarb.viewBridgeClasses.ViewBridge.prototype.attachEvents.call(this);
};

window.rhubarb.viewBridgeClasses.FeedsCollectionViewBridge = bridge;
