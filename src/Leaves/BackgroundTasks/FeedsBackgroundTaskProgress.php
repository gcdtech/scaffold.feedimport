<?php
namespace Rhubarb\Scaffolds\FeedImport\Leaves\BackgroundTasks;

use Rhubarb\Scaffolds\BackgroundTasks\Leaves\BackgroundTaskProgress;
use Rhubarb\Scaffolds\BackgroundTasks\Models\BackgroundTaskStatus;

/**
 * Class FeedsBackgroundTaskProgress
 * @package Rhubarb\Scaffolds\FeedImport\Leaves\BackgroundTasks
 */
class FeedsBackgroundTaskProgress extends BackgroundTaskProgress
{
    protected function getViewClass()
    {
        return FeedsBackgroundTaskProgressView::class;
    }

    protected function onModelCreated()
    {
        $this->model->getProgressEvent->attachHandler( function ()
        {
            $status = new BackgroundTaskStatus( $this->model->backgroundTaskStatusId );

            $progress = new \stdClass();
            $progress->percentageComplete = $status->PercentageComplete;
            $progress->message = $status->Message;
            $progress->isRunning = $status->isRunning();
            $progress->taskStatus = $status->TaskStatus;

            $status->Message = "";
            $status->save();

            return $progress;
        } );
    }
}