var bridge = function (presenterPath) {
    window.rhubarb.viewBridgeClasses.BackgroundTaskProgressViewBridge.apply(this, arguments);
    window.rhubarb.viewBridgeClasses.BackgroundTaskViewBridge.pollRate = 500;
};

bridge.prototype = new window.rhubarb.viewBridgeClasses.BackgroundTaskProgressViewBridge();
bridge.prototype.constructor = bridge;

bridge.prototype.onProgressReported = function (progress) {
    this.progressNode.style.width = progress.percentageComplete + "%";
    if( progress.message != "" ) {
        this.messageNode.innerHTML += progress.message;
    }
};


window.rhubarb.viewBridgeClasses.FeedsBackgroundTaskProgressViewBridge = bridge;