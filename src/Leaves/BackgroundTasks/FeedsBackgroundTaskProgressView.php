<?php
namespace Rhubarb\Scaffolds\FeedImport\Leaves\BackgroundTasks;

use Rhubarb\Scaffolds\BackgroundTasks\Leaves\BackgroundTaskProgressView;

/**
 * @author John
 * Date: 22/09/2015
 */
class FeedsBackgroundTaskProgressView extends BackgroundTaskProgressView
{
    /**
     * @return \Rhubarb\Leaf\Leaves\PresenterDeploymentPackage
     */
    public function getDeploymentPackage()
    {
        $package = parent::getDeploymentPackage();
        $package->resourcesToDeploy[] = __DIR__ . '/FeedsBackgroundTaskProgressViewBridge.js';

        return $package;
    }

    /**
     * @return string
     */
    protected function getViewBridgeName()
    {
        return "FeedsBackgroundTaskProgressViewBridge";
    }

}