<?php
/**
 * @author John
 * Date: 23/09/2015
 */

namespace Rhubarb\Scaffolds\FeedImport\BackgroundTasks;

use Rhubarb\Scaffolds\BackgroundTasks\Models\BackgroundTaskStatus;
use Rhubarb\Stem\Repositories\MySql\Schema\Columns\MySqlMediumTextColumn;
use Rhubarb\Stem\Schema\ModelSchema;

/**
 *
 *
 * @property int $BackgroundTaskStatusID Repository field
 * @property string $TaskClass Repository field
 * @property string $TaskStatus Repository field
 * @property float $PercentageComplete Repository field
 * @property string $Message Repository field
 * @property string $ExceptionDetails Repository field
 * @property \stdClass $TaskSettings Repository field
 */
class FeedBackgroundTaskStatus extends BackgroundTaskStatus
{

    /**
     * @param ModelSchema $schema
     */
    protected function extendSchema( ModelSchema $schema )
    {
        $schema->addColumn( new MySqlMediumTextColumn( "Message" ));
        parent::extendSchema( $schema );
    }

}