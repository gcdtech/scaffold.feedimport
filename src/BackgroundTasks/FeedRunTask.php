<?php

namespace Rhubarb\Scaffolds\FeedImport\BackgroundTasks;

use Exception;
use Rhubarb\Scaffolds\BackgroundTasks\BackgroundTask;
use Rhubarb\Scaffolds\BackgroundTasks\Models\BackgroundTaskStatus;
use Rhubarb\Scaffolds\FeedImport\Engine\FeedController;

class FeedRunTask extends BackgroundTask
{

    /**
     * @param BackgroundTaskStatus $status
     */
    public function execute( BackgroundTaskStatus $status )
    {
        // save the status to ensure we have the ProcessID stored in DB
        $status->Message .= "Starting Background Task - " . date("Y-m-d H:i:s");
        $status->save();

        try {
            $feedId = $status->TaskSettings[ 'FeedID' ];
            $feed = new FeedController( $feedId );
            FeedController::$feed->setProcessInfo( $status->BackgroundTaskStatusID, date( "Y-m-d H:i:s" ) );
            $feed->process();
        } catch( Exception $ex ) {
            $status->Message .= $ex->getMessage();
            $status->TaskStatus = FeedBackgroundTaskStatus::TASK_STATUS_FAILED;
            $status->save();
        }
    }

    /**
     * appends a log message to background task status
     * (using Message as buffer)
     *
     * @param $taskId
     * @param $message
     */
    public static function logMessage( $taskId, $message )
    {
        $status = new FeedBackgroundTaskStatus( $taskId );
        $status->reload();
        $status->Message .= $message . "<BR />\n";

        // if the Message has grown as large as 2500 characters, the browser isn't watching so cut it off
        if( strlen( $status->Message ) > 2500 )
        {
            $status->Message = "";
        }

        $status->save();
    }
}