var bridge = function( leafPath )
{
	window.rhubarb.viewBridgeClasses.TimeViewBridge.apply( this, arguments );
}

bridge.prototype = new window.rhubarb.viewBridgeClasses.TimeViewBridge();
bridge.prototype.constructor = bridge;

bridge.prototype.attachEvents = function()
{
	var self = this;

	if( $( "#FeedsSettings_EnableScheduling:checked" ).length == 0 )
	{
		$( "#" + this.leafPath + '_WhenToRun' ).hide();
		$( "#" + this.leafPath + '_hours' ).hide();
		$( "#" + this.leafPath + '_minutes' ).hide();
	}

	if( $( "#" + self.leafPath + '_WhenToRun' ).val() != "Daily" )
	{
		$( "#" + this.leafPath + '_hours' ).hide();
		$( "#" + this.leafPath + '_minutes' ).hide();
	}

	if( $( "#" + this.leafPath + '_WhenToRun' ).val() == "Custom" )
	{
		$( "#" + this.leafPath + '_hours' ).show();
		$( "#" + this.leafPath + '_minutes' ).hide();
	}

	$( "#" + this.leafPath + '_WhenToRun' ).change( function( event )
	{
		if( this.value == "Daily" )
		{
			$( "#" + self.leafPath + '_hours' ).show();
			$( "#" + self.leafPath + '_minutes' ).show();
		}
		else if( this.value == "Custom" )
		{
			$( "#" + self.leafPath + '_hours' ).show();
			$( "#" + this.leafPath + '_minutes' ).hide();
		}
		else
		{
			$( "#" + self.leafPath + '_hours' ).hide();
			$( "#" + self.leafPath + '_minutes' ).hide();
		}
	} );

	$( "#FeedsSettings_EnableScheduling" ).click( function( event )
	{
		$( "#" + self.leafPath + '_hours' ).hide();
		$( "#" + self.leafPath + '_minutes' ).hide();

		if( this.checked )
		{
			$( "#" + self.leafPath + '_WhenToRun' ).show();
			if( $( "#" + self.leafPath + '_WhenToRun' ).val() == "Daily" )
			{
				$( "#" + self.leafPath + '_hours' ).show();
				$( "#" + self.leafPath + '_minutes' ).show();
			}
			else if( $( "#" + self.leafPath + '_WhenToRun' ).val() == "Custom" )
			{
				$( "#" + self.leafPath + '_hours' ).show()
				$("#" + this.leafPath + '_minutes').hide();
			}
		}
		else
		{
			$( "#" + self.leafPath + '_WhenToRun' ).hide();
		}
	} );

	// Assign a value to toggled feed settings checkboxes
	$( "form input[type=checkbox]" ).change( function()
	{
		var checkbox = $( this );
		if( checkbox.is( ":checked" ) )
		{
			checkbox.val( "1" );
			checkbox.parent().find( "input[type=hidden]" ).val( "1" );
		}
		else
		{
			checkbox.val( "0" );
			checkbox.parent().find( "input[type=hidden]" ).val( "0" );
		}
	} );

	window.rhubarb.viewBridgeClasses.TimeViewBridge.prototype.attachEvents.call( this );
};

window.rhubarb.viewBridgeClasses.ScheduleTimeDropDownViewBridge = bridge;