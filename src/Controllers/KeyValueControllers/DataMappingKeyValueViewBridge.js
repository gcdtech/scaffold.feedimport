var bridge = function( leafPath )
{
	window.rhubarb.viewBridgeClasses.KeyValueViewBridge.apply( this, arguments );
}

bridge.prototype = new window.rhubarb.viewBridgeClasses.KeyValueViewBridge();
bridge.prototype.constructor = bridge;

bridge.prototype.attachEvents = function()
{
	window.rhubarb.viewBridgeClasses.KeyValueViewBridge.prototype.attachEvents.call(this);
};

window.rhubarb.viewBridgeClasses.DataMappingKeyValueViewBridge = bridge;