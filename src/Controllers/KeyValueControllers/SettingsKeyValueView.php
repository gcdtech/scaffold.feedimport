<?php

namespace Rhubarb\Scaffolds\FeedImport\Controllers\KeyValueControllers;

class SettingsKeyValueView extends KeyValueView
{
    public function setValues( $values )
    {
        if (empty( $this->values ) && empty( $values )) {
            $this->values = array(
                "File Name"            => "",
                "File Always Required" => "",
                "File URL"             => "",
                "Enable Data Tracking" => "",
                "Is Incremental"       => "",
                "Old Site Prefix"      => ""
            );
        }
        if (!empty( $values )) {
            $this->values = $values;
        }

        parent::setValues( $this->values );
    }

    protected function printViewContent()
    {
        ?>
        <div class="settings-key-value-pairs">
            <?php
            if ($this->values) {
                foreach ($this->values as $key => $value) {
                    if ($key != "Schedule") {
                        $this->printLine( $key, $value );
                    }
                }
            }
            ?>
        </div>
    <?php
    }

    public function printLine( $key, $value )
    {
        $type = "text";
        $checked = "";

        if ($key == "File Always Required" || $key == "Enable Data Tracking" || $key == "Is Incremental") {
            $type = "checkbox";

            if ($value == "on") {
                $checked = 'checked = "checked"';
            }
        }

        print '<div class="key-val">
                <label id="' . $this->model->leafPath . '_key[' . $key . ']" for="' . $this->model->leafPath . '_value[' . $key . ']">' . $key . '</label>
                <input style="display: none;" id="' . $this->model->leafPath . '_key[' . $key . ']" name="' . $this->model->leafPath . '_key[' . $key . ']" value="' . htmlentities( $key ) . '">
                <input id="' . $this->model->leafPath . '_value[' . $key . ']" name="' . $this->model->leafPath . '_value[' . $key . ']" value="' . htmlentities( $value ) . '" type="' . $type . '"' . $checked . '>';
        print '<br></div>';
    }

    public function printAddButton()
    {
    }
}