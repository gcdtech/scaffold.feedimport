<?php

namespace Rhubarb\Scaffolds\FeedImport\Controllers\KeyValueControllers;

class DataMappingKeyValue extends KeyValue
{
    protected function getViewClass()
    {
        return DataMappingKeyValueView::class;
    }
}