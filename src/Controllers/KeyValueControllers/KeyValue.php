<?php

namespace Rhubarb\Scaffolds\FeedImport\Controllers\KeyValueControllers;

use Rhubarb\Leaf\Leaves\Controls\Control;

class KeyValue extends Control
{
    protected function getViewClass()
    {
        return KeyValueView::class;
    }

    protected function onModelCreated()
    {
        parent::onModelCreated();

        $this->model->value = [];
    }
}