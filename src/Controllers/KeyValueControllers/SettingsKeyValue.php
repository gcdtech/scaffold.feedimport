<?php

namespace Rhubarb\Scaffolds\FeedImport\Controllers\KeyValueControllers;

class SettingsKeyValue extends KeyValue
{
    protected function getViewClass()
    {
        return SettingsKeyValueView::class;
    }
}