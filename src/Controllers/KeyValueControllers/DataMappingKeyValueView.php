<?php

namespace Rhubarb\Scaffolds\FeedImport\Controllers\KeyValueControllers;

class DataMappingKeyValueView extends KeyValueView
{
    public function getDeploymentPackage()
    {
        $package = parent::getDeploymentPackage();
        $package->resourcesToDeploy[ ] = __DIR__ . "/DataMappingKeyValueViewBridge.js";

        return $package;
    }

    protected function getViewBridgeName()
    {
        return "DataMappingKeyValueViewBridge";
    }
}