<?php

namespace Rhubarb\Scaffolds\FeedImport\Controllers\KeyValueControllers;

use Rhubarb\Crown\Deployment\RelocationResourceDeploymentProvider;
use Rhubarb\Crown\Request\WebRequest;
use Rhubarb\Leaf\Leaves\Controls\ControlView;
use Rhubarb\Leaf\Leaves\LeafDeploymentPackage;

class KeyValueView extends ControlView
{
    protected $requiresContainerDiv = true;

    public function getDeploymentPackage()
    {
        return new LeafDeploymentPackage(
            VENDOR_DIR . "/components/jquery/jquery.min.js",
            __DIR__ . "/KeyValueViewBridge.js");
    }

    protected function printViewContent()
    {
        ?>
        <xmp id="hidden-line-placeholders" style="display: none;"><?php $this->printLine( '', '' ) ?></xmp>
        <div class="key-value-pairs">
            <?php
            if ($this->model->value) {
                foreach ($this->model->value as $key => $value) {
                    $this->printLine( $key, $value );
                }
            }
            ?>
        </div>
        <?php
        $this->printAddButton();
    }

    public function printAddButton()
    {
        print '<a href="#" id="keyAddText">Add</a>';
    }

    public function printRemoveButton()
    {
        print '<a href="#" id="keyRemoveText">Remove</a>';
    }

    public function printLine( $key, $value )
    {
        print '<div class="key-val">
                <input id="' . $this->model->leafPath . '_key[' . $key . ']" name="' . $this->model->leafPath . '_key[' . $key . ']" type="text" value="' . htmlentities( $key ) . '">
                <input id="' . $this->model->leafPath . '_value[' . $key . ']" name="' . $this->model->leafPath . '_value[' . $key . ']" type="text" value="' . htmlentities( $value ) . '">';
        $this->printRemoveButton();
        print '<br></div>';
    }

    protected function getViewBridgeName()
    {
        return "KeyValueViewBridge";
    }

    protected function parseRequest( WebRequest $request )
    {
        $keys = $request->post( $this->model->leafPath . "_key" );
        $values = $request->post( $this->model->leafPath . "_value" );

        $data = [ ];

        if( $keys )
        {
            foreach( $keys as $key => $value )
            {
                // Checking if there is a value for each key, as checkbox values of 0 do not get sent through in the POST data.
                // If it does not exist, set the value to be blank.
                if( array_key_exists( $key, $values ) )
                {
                    $data[ $value ] = $values[ $key ];
                }
                else
                {
                    $data[ $value ] = "";
                }
            }
            $this->setControlValueForIndex(null, $data);
        }
    }
}