<?php

namespace Rhubarb\Scaffolds\FeedImport\Controllers\KeyValueControllers;

use Rhubarb\Leaf\Leaves\LeafDeploymentPackage;

class ReportsKeyValueView extends KeyValueView
{
    public function getDeploymentPackage()
    {
        return new LeafDeploymentPackage( __DIR__ . "/ReportsKeyValueViewBridge.js" );
    }

    public function printLine( $key, $value )
    {
        $reportOptions = "";
        $options = array(
            "All"       => "All",
            "All GCD"   => "AllGCD",
            "Dealer"    => "Dealer",
            "Developer" => "Developer",
            "GCD"       => "GCD",
            "No Data"   => "NoData"
        );

        foreach ($options as $title => $option) {
            $selected = "";
            if ($option == $value) {
                $selected = "selected='selected'";
            }

            $reportOptions .= "<option value='{$option}'{$selected}>{$title}</option>";
        }

        print '<div class="key-val">
                <input id="' . $this->model->leafPath . '_key[' . $key . ']" name="' . $this->model->leafPath . '_key[' . $key . ']" type="text" value="' . htmlentities( $key ) . '">
                <select id="' . $this->model->leafPath . '_value[' . $key . ']" name="' . $this->model->leafPath . '_value[' . $key . ']">' . $reportOptions . '</select>';
        $this->printRemoveButton();
        print '<br></div>';
    }

    protected function getViewBridgeName()
    {
        return "ReportsKeyValueViewBridge";
    }
}