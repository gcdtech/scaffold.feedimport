<?php

namespace Rhubarb\Scaffolds\FeedImport\Controllers\KeyValueControllers;

class ReportsKeyValue extends KeyValue
{
    protected function getViewClass()
    {
        return ReportsKeyValueView::class;
    }
}