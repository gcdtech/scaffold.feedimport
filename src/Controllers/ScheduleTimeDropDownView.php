<?php

namespace Rhubarb\Scaffolds\FeedImport\Controllers;

use Rhubarb\Leaf\Controls\Common\DateTime\TimeView;
use Rhubarb\Leaf\Controls\Common\SelectionControls\DropDown\DropDown;
use Rhubarb\Leaf\Leaves\LeafDeploymentPackage;

class ScheduleTimeDropDownView extends TimeView
{
    public function createSubLeaves()
    {
        $this->registerSubLeaf( $whenToRun = new DropDown( "WhenToRun" ) );

        $values = [
                [ "0" => "Never", "1" => "Never" ],
                [ "0" => "+15 minutes", "1" => "Run Every 15 minutes" ],
                [ "0" => "+30 minutes", "1" => "Run Every 30 minutes" ],
                [ "0" => "+1 hour", "1" => "Run Hourly" ],
                [ "0" => "+6 hours", "1" => "Run Every 6 hours" ],
                [ "0" => "Daily", "1" => "Daily at:" ],
                [ "0" => "Custom", "1" => "Run Every X hours" ]
        ];
        $whenToRun->setSelectionItems(
                [
                        "" => $values
                ] );

        parent::createSubLeaves();
    }

    public function printViewContent()
    {
        print $this->leaves[ "WhenToRun" ] . " " . $this->leaves[ "hours" ] . " " . $this->leaves[ "minutes" ];
    }

    protected function getViewBridgeName()
    {
        return "ScheduleTimeDropDownViewBridge";
    }

    public function getDeploymentPackage()
    {
        $package = parent::getDeploymentPackage();
        $package->resourcesToDeploy[] = __DIR__ . "/ScheduleTimeDropDownViewBridge.js";

        return $package;
    }
}