<?php

namespace Rhubarb\Scaffolds\FeedImport\Controllers;

use Rhubarb\Leaf\Controls\Common\DateTime\Time;

class ScheduleTimeDropDown extends Time
{
    public function __construct( $name, $defaultValue = null )
    {
        parent::__construct( $name, $defaultValue, 15, 0, 23 );
    }

    protected function getViewClass()
    {
        return ScheduleTimeDropDownView::class;
    }

    protected function parseCompositeValue( $compositeValue )
    {
        if ( preg_match("/^Daily ([0-9]{2})([0-9]{2})$/", $compositeValue, $match ) ) {
            $this->model->hours = $match[1];
            $this->model->minutes = $match[2];
            $this->model->WhenToRun = "Daily";
        } elseif ( preg_match( "/^Custom ([0-9]{2})$/", $compositeValue, $match ) ) {
            $this->model->hours = $match[1];
            $this->model->WhenToRun = "Custom";
        } else {
            $this->model->WhenToRun = $compositeValue;
        }
    }

    protected function createCompositeValue()
    {
        if ( $this->model->WhenToRun == "Daily" ) {
            return "Daily " . $this->model->hours . $this->model->minutes;
        } elseif ( $this->model->WhenToRun == "Custom" ) {
            return "Custom " . $this->model->hours;
        } else {
            return $this->model->WhenToRun;
        }
    }

    protected function createModel()
    {
        return new ScheduleTimeDropDownModel();
    }

}