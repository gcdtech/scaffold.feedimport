<?php
/**
 * @author simongough
 * Date: 08/06/2016
 */

namespace Rhubarb\Scaffolds\FeedImport\Controllers;

use Rhubarb\Leaf\Controls\Common\DateTime\TimeModel;

class ScheduleTimeDropDownModel extends TimeModel
{
	public $WhenToRun = "Never";
}