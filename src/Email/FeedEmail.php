<?php

namespace Rhubarb\Scaffolds\FeedImport\Email;

use Rhubarb\Crown\Sendables\Email\SimpleEmail;
use Rhubarb\Crown\Sendables\SendableProvider;

class FeedEmail extends SimpleEmail
{
    function __construct( $subject = "", $text = "", $html = "" )
    {
        $this->setSubject($subject);
        $this->setText($text);
        $this->setHtml($html);
    }

    public final function send()
    {
        $this->logSending();

        SendableProvider::selectProviderAndSend( $this );
    }
}
